*****
Quota
*****

Aktiválás
=========

Az '''oned.conf''' fájlban kell beállítani.

.. TODO: Indent lines, set language: Example .. code-block:: python
AUTH_MAD = [
    executable = "one_auth_mad",
    arguments  = "--authz quota --authn ssh,x509,ldap,server_cipher,server_x509"
]


MySQL
=====
Az '''auth/quota.conf''' -ban lehet beállítani.

.. TODO: Indent lines, set language: Example .. code-block:: python
:db: mysql://oneadmin:oneadmin@localhost/opennebula

