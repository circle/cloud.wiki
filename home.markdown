Általános fejlesztői leírások
======
 * [Fejlesztői környezet](DevEnv)
 * [Fejlesztés menete](DelopmentStyle)
 * [Stílus elvárások](CodingStyle)
 * [Tesztelési elvárások](Teszteles)
 * [Nehéz problémák megoldásai](Szivasok)

Infrastruktúra
------
 * [IK felhő hardvere](MasodikEmeletiRack)
 * [VPN szerver](vpn)
 * [Linksys switch kezelése](LinksysSwitch)
 * [Nagios monitorozás](NagiosMonitorozas)
 * [Buildbot](Buildbot)
 * [ILO](Managementprocessors)
 * [Logolás](logolas)

CIRCLE Cloud
------
 * [Felhasználói feltételek](FelhasznaloiFeltetelek)
 * [Felhasználói leírás](UserGuide)
 * [Fejlesztői alapinformációk](CircleDevelopment)
 * [Tűzfal tudnivalók](TuzfalTudnivalok)
 * [saml2 beállítása](djangosaml2)
 * [Selenium end-to-end tesztelés](roadtotestenv)

### Guest gépek
 * [Linux](GuestLinux)
   * [Ubuntu/Debian(hpacucli is itt van)](Ubuntu-Debian)  [további cuccok](ubuntuhasznos)
   * [CentOS/RedHat](CentOS/RedHat)
   * Suse
   * [Diszk átméretezése](fsresize)
 * Windows
   * Windows XP
   * [Windows 7](GuestWindows7)
 * [PCI passthrough](PCIPassthrough)
 * [ host hálókártya közvetlen használata (macvtap)](macvtap)


***
Random
------
[PortálÖtletek](PortalOtletek)
[LaborÖtletek](LaborOtletek)

[bd ideiglenes cuccai](bd ideiglenes cuccai)

Érdekességek
------
 * http://www.coned.utcluj.ro/GreenCloudScheduler/Documentation.html#Deployment - GreenCloud Scheduler with WakeOnLan
 * http://blog.opennebula.org/?p=2695 - Elastic IP with OVSwitch
