= BME-NET =

== ARP bevezető ==

`man 7 arp <http://www.kernel.org/doc/man-pages/online/pages/man7/arp.7.html>`_

`ARP áttekintés <http://linux-ip.net/html/ether-arp.html>`_

Infrastruktúra
==============

.. TODO: Indent lines, set language: Example .. code-block:: python
                 ________
[Host - 01]-----[        ]                           .--------.
[Host - 02]-----[ Switch ]-----[ Router (fw1) ]-----| INTERNET |
[Host - ..]-----`________ <________>`_                           '--------'



Programok
=========

send_arp
--------
Preparált ARP csomagok küldésére alkalmas program.

arp_daemon
----------
A /var/tmp/one/ip_proxy mappába helyezett fájlok neveit folyamatosan frissíti Gratuitous ARP üzenetek segítségével.
Az itt elhelyezett fájloknak szabványos pontokkal elválasztott IP címeknek kell lenniük pl: 152.66.243.100.

VNM/ovswitch (Opennebula network hook)
--------------------------------------
Három állapotot különböztetünk meg:


* pre - VM indítása elötti inicializáció


* post - VM inditás utáni élesítés
  Fájlok létrehozása az arp_daemonnak.

.. TODO: Indent lines, set language: Example .. code-block:: python
  require 'OpenvARP'
  ...
  onevarp = OpenvSwitchARP.from_base64(template64, deploy_id)
  onevarp.proxy_add



* clean - VM törlés / migrate stb. után takarítás
  Fájlok törlése az arp_daemon mappájából

.. TODO: Indent lines, set language: Example .. code-block:: python
  require 'OpenvARP'
  ...
  onevarp = OpenvSwitchARP.from_base64(template64, deploy_id)
  onevarp.proxy_del


=== OpenvARP.rb ===


.. TODO: Indent lines, set language: Example .. code-block:: python
require 'OpenNebulaNetwork'

class OpenvSwitchARP < OpenNebulaNetwork
    XPATH_FILTER = "TEMPLATE/NIC[BRIDGE='bme-net']"

    def initialize(vm, deploy_id = nil, hypervisor = nil)
        super(vm,XPATH_FILTER,deploy_id,hypervisor)
    end

    def proxy_add
        process do |nic|
            path = "/var/tmp/one/proxy_ip/" + nic`:ip <:ip>`_;
            #puts path;
            File.open(path, 'w')
        end
    end
    def proxy_del
        process do |nic|
            path = "/var/tmp/one/proxy_ip/" + nic`:ip <:ip>`_;
            if File.exist?(path)
                #puts path
                File.delete(path);
            end
        end
    end

end



Módosítások a kernel ARP beállításain
=====================================

Az ARP cache bejegyzések a base_reachable_time/2 és 3*base_reachable_time/2 időszak között véletlenszerűen évülnek el.

Ha megemeljük 60 000msec-re akkor legrosszabb esetben is 30 másodpercig a táblában marad.

.. TODO: Indent lines, set language: Example .. code-block:: python
echo "60000" > /proc/sys/net/ipv4/neigh/vlan0006/base_reachable_time_ms


Ahhoz, hogy a Gratuitous ARP-ot használja az ARP cache engedélyeznünk kell, az adott interfészen.

.. TODO: Indent lines, set language: Example .. code-block:: python
echo 1 > /proc/sys/net/ipv4/conf/vlan0006/arp_accept


Ezt követően az arp_daemonnak legalább 30 másodpercenként broadcastolnia kell 1 címet. Az ARP csomagok között "illik" 1 másodperc szünetet hagyni.

= BME-NET v2 =

Infrastruktúra
==============

Ebben az esetben a routolást a mega3 végzi. Ilyenkor kézzel statikus ARP táblát generálunk a 152.66.243.81-152.66.243.94 tartományra.

.. TODO: Indent lines, set language: Example .. code-block:: python
                 ________
[Host - 01]-----[        ]                           .--------.
[Host - 02]-----[ Switch ]-----[ Mega3 (router) ]---| INTERNET |
[Host - ..]-----`________ <________>`_                           '--------'



Konfigurálás
============
A mega3-on egy vlan taggelt interfacet adunk a vm-net bridge-hez.

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo ovs-vsctl add-br bme-net vm-net 2

Az interface felkonfigurálása

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo ifconfig bme-net 152.66.243.81 netmask 255.255.255.0

A route táblában erre az interface-re továbbítjuk a bme tartományt.

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo route add -net 152.66.243.80 netmask 255.255.255.240 dev bme-net

A DHCP servert nekünk kell beállítanunk az interface-re

.. TODO: Indent lines, set language: Example .. code-block:: python
TODO

Opennebula
==========
bme-net network felvétele a fenti tartománnyal. Bridge-nek a vm-net-et adjuk. VLAN tag-nek pedig az elöbb beállított VLAN-taget.