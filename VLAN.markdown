***********************
VLAN hálózat beállítása
***********************

MTU 1504
========
A Host gépeken a switch-be menő port MTU-ját (ez a bridgelt interface) meg kell növelni, hogy a VLAN tag miatt 4 byte-al megnövekedett csomagok is át tudjanak menni.

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo ifconfig eth0 mtu 1504


Ez most már nem kell valamiért.

Ha a 2. emeleti switchen új vlant adunk hozzá, be kell kapcsolni külön a jumbópakázst.