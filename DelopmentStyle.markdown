A forrásfa tárolása a gitlab (https://git.ik.bme.hu/) kezelésében lévő git repositorykban, a fejlesztéssel kapcsolatos nyilvántartásokat a gitlabban vezetjük.

# Új funkció fejlesztése

Amennyiben új funkciót írsz, vagy egy összetettebb issue-n dolgozol, amint van említésre méltó commitolható kódod, azt töltsd föl egy külön branch-be (feature-bla-bla vagy issue-1234 néven). Ezután hozz létre egy *merge requestet* a gitlabon, ahol a többiek tudják jelezni, ha valami nem tetszik nekik (code review), vagy leírhatják az általuk végzett tesztelés eredményét. (A masteren kívüli branchekre is lefut a buildbotban a teszt, amit egyelőre csak a buildbot webes felületén és ircen látni. Bug, hogy a vadiúj branchekről nem kap értesítést a buildbot, külön kell neki szólni.)

Ha a funkció ki van próbálva, és nincs lezáratlan kérdés, akkor lehet merge-elni a master ágba (lehetőleg ne az, aki a kódot írta).