Az Ubuntu 12.04-ben lévő nginx nem támogatja a websocket proxyzását, emiatt az nginx repóját kell használni:

```sh

wget http://nginx.org/keys/nginx_signing.key -O - | apt-key add -
echo "deb http://nginx.org/packages/ubuntu/ precise nginx" > /etc/apt/sources.list.d/nginx.list
apt-get update && apt-get install nginx
```

```sh
cp ~/circle/miscellaneous/nginx-uwsgi.conf /etc/nginx/conf.d/default.conf
/etc/init.d/nginx restart
```

django:

```~/.virtualenvs/circle/bin/postactivate``` fájlban szerepeljen ez a sor:

```sh
DJANGO_SETTINGS_MODULE="circle.settings.production"
```

```sh
stop mancelery; 
workon circle
pip install -r requirements/production.txt 
cd ~/circle/circle
./manage.py collectstatic
./manage.py compilemessages
cp ~/circle/miscellaneous/portal-uwsgi.conf /etc/init/portal.conf
start portal
start mancelery
```