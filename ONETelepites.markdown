*********************
Opennebula telepítése
*********************

== Csomagból elérhető telepítés ==

http://opennebula.org/

== Forrásból történő telepítés ==
=== Függőségek telepítése ===

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo apt-get install ruby libsqlite3-dev ssh libxmlrpc-c3-dev g++ scons libssl-dev libxml2-dev libmysqlclient-dev libsqlite3-dev mkisofs

Opcionális csomag mysql esetén

.. TODO: Indent lines, set language: Example .. code-block:: python
apt-get install mysql-server # rootpass=Almafa12
apt-get install libmysqlclient-dev


Fordítás
--------
A fordítés *scons*-al történik. scons [OPTION=VALUE]

.. TODO: Indent lines, set language: Example .. code-block:: python
scons mysql=yes


Telepítés
---------
Létre kell hozni az **oneadmin** user-t és a **cloud** csoportot

.. TODO: Indent lines, set language: Example .. code-block:: python
TODO

Az oneadmin user-t hozzá kell adni a kvm csoporthoz. Megj.: Erre azért van szükség, mert a libvirt oneadmin:cloud jogosultságokkal fogja futtatni a VM-eket és a kvm group fér hozzá a /dev/kvm driverekhez.

.. TODO: Indent lines, set language: Example .. code-block:: python
TODO

Az install script segítségével.

.. TODO: Indent lines, set language: Example .. code-block:: python
./install.sh -u oneadmin -g cloud -d /var/lib/opennebula


Sunstone Web adminisztráció
---------------------------
Az install script segítségével.

.. TODO: Indent lines, set language: Example .. code-block:: python
./install.sh -u oneadmin -g cloud -d /var/lib/opennebula -s

