Hálózati kártya átadása közvetlenül a virtuális gépnek (pl. forgalom megfigyelése):

```
<devices>
  <interface type='direct'>
    <source dev='em3' mode='passthrough'/>
    <model type='virtio'/>
    <mac address='00:17:A4:3A:95:9A'/>
  </interface>
</devices>
```