
telepítés
=========

* `apt-get install krb5-user`
* válaszok: IK.LOCAL, 10.2.1.1, 10.2.1.1

használat
=========

* kerberos ticket kérése: `kinit`     (ha a helyi és a kerberos-os user nem ugyanaz, akkor `kinit user@IK.LOCAL` (fontos, hogy IK.LOCAL nagybetűs legyen))
* ssh-zni hosztnévvel lehet csak!