'''Az /etc/rc.local-t elég későn hajtja végre, így néha login közben frissül a home.'''

Megoldás:

* /etc/rc.local-ból kitöröljük a CUCCOT
* `/etc/init/cleanup.conf`-ba:

.. TODO: Indent lines, set language: Example .. code-block:: python
start on filesystem

pre-start script
    rm -rf /home/labor
    cp -rp /root/labor /home/ # ez nem biztos, majd meg kell nézni, fejből mirctam
end script


* '''Ki kell próbálni! '''
