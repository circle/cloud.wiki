*********
Hálózatok
*********

'''Mindezt az fw2 csinálja már. Régi cuccokról:'''


DHCP
====

DHCPD
-----

DHCP server

Host-file

.. TODO: Indent lines, set language: Example .. code-block:: python
host VM-1 {
  option host-name "VM-1";
  hardware ethernet 02:00:0a:07:05:02;
  fixed-address 192.168.1.55;
}


Config file

.. TODO: Indent lines, set language: Example .. code-block:: python
subnet 192.168.1.0 netmask 255.255.255.0 {
  range 192.168.1.50 192.168.1.254;
}

include "/etc/dhcp/dhcpd-static.conf";


Parancs:

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo dhcpd <iface>


DNSMASQ
-------

Egyszerű dhcp és dns cache egyben (router lesz a dns server)


.. TODO: Indent lines, set language: Example .. code-block:: python
sudo /usr/sbin/dnsmasq --strict-order --bind-interfaces --pid-file=/home/aaa/vm-net.pid --conf-file= --except-interface lo --listen-address 192.168.1.1 --dhcp-range 192.168.1.1,static --dhcp-no-override --dhcp-hostsfile=/home/aaa/vm-net.hostfile


Hostfile

<mac>,<ip>,<host>

.. TODO: Indent lines, set language: Example .. code-block:: python
02:00:0a:07:05:02,192.168.1.10,vm-10

