# Gyakori műveletek

Belépés telneten
================

`linksys 10.3.0.1` (általam barkácsolt script, ami megcsinálja a lenti melót)
ha ez nem menne, akkor:


* telnet 10.3.0.1 (ez pl az l3-ik1)
* user: admin pass: szokásos
* enter
* **CTRL+Z**, lcli beírása, jelszó újra
* elvileg egy shellt kapunk

Belépés soros terminálon
========================

* keressünk egy linksys felirattal ellátott kábelt (fehér kábel, pirossal van ráírva a csatlakozóra)
* a kábel gyakori előfordulási helyei:

   * bd fiókjában
   * bd mögötti szekrény
   * szerverszobában a másodikon vagy a harmadikon (valamelyik switch-re dugva)

* 38400 8n1 pl: `screen /dev/ttyS0 38400`

Port felkonfigolása egy adott vlanra
====================================
Ha azt szeretnénk, hogy az e11-es **porton** 5-ös **vlan legyen**, akkor a következőt kell beírni:

```
config
 interface ethernet e11
  switchport mode access
  switchport access vlan 5
 exit
exit
```
Ha a port access modban van akkor a `switchport mode access` kihagyható.
 
Portok állapotának lekérdezése
==============================
Ha szeretnénk tudni, hogy **melyik porton van link**, akkor a következőt kell beírni:

`show interfaces status`


Melyik PORT melyik VLAN
=======================
Ha szeretnénk tudni, hogy a **portokon milyen vlan** van felkonfigurálva, akkor a következőt kell beírni:

`show vlan`


Egy gép, ami rosszalkodik, de nem tudod hol van
=======================

* megtudod a mac címét, pl arp tábla
* `show bridge address-table`

Port visszakapcsolása, illetve minden más, ahol **no** kell a parancs elé
=======================

```
config
 interface ethernet e11
  no shutdown
 exit
exit
```

# Hasznos dolgok

Vlanok hozzááadása
==================

```
config
 vlan database
  vlan 2-13,255
 exit
exit
```

Vlan elnevezése
=================

A vlan-okat célszerű elenevezni, mert abból sejthető mire használjuk.
```
config
 interface vlan 5
  name kedvenc_vlanom
 exit
exit
```

Uplink beállítása
=================

```
config
 interface ethernet g1
  switchport mode general
  no switchport general acceptable-frame-type tagged-only
  switchport general pvid 255
  switchport general allowed vlan add 2-13
  description uplink
 exit
exit
```
Ha már előzőleg is fel volt konfigurálva és csak vlan-t akarsz hozzáadni akkor ennyi is elég:
```
config
 interface ethernet g1
  switchport general allowed vlan add 2-13
 exit
exit
```

RSTP belövése
=================

```
config
 spanning-tree
 spanning-tree mode rstp
exit
```

A kliensportokat állítsuk be portfast módba (így gyorsabban kerül FWD állapotba a port):

```
config
 interface range ethernet e(1-24)
  spanning-tree portfast
 exit
exit
```

ACL felhúzása a portra
======================

```
config
 ip access-list e11
  deny-udp 0.0.0.0 255.255.255.255 67 255.255.255.255 0.0.0.0 68
  permit-udp 0.0.0.0 0.0.0.0 68 255.255.255.255 0.0.0.0 67
  permit any 10.5.253.6 0.0.0.0 any
 exit
 interface ethernet e11
  service-acl input e11
 exit
exit
```

menedzsment hálózat
===================

```
config
 management vlan 3
 interface vlan 3
  ip address 10.3.0.1 255.255.0.0
 exit
 ip default-gateway 10.3.255.254
exit
```

NTP szerver
=========

```
config
 sntp unicast client enable
 sntp unicast client poll
 sntp server 10.3.255.254
exit
```

Log
===

```
config
 log 152.66.243.97
exit
```

Ne dobáljon ki egy idő után
===========================

```
config
 line telnet
  exec-timeout 0
 exit
exit
```

Firmware frissítése
===================

* `copy tftp://10.3.3.4/SRW2xxG4_FW_1.2.3.0.ros image`
* `reload`

Brickeltem, nem bootol a switch, hibás fw-re panaszkodik
==========================================

* linksys feliratú söröskábellel kössük össze
* http://ericrochow.wordpress.com/2012/09/29/upload-an-ios-image-via-xmodem/

SNMP
====

vlan: 1.3.6.1.2.1.17.7.1.4.3.1.1

port pvid: 1.3.6.1.2.1.17.7.1.4.5.1.1

untagged portok: 1.3.6.1.2.1.17.7.1.4.3.1.4

port sebessége: 1.3.6.1.2.1.2.2.1.5

számlálók: 1.3.6.1.2.1.2.2.1.10

LLDP belövése:

```bash
snmpset -v2c -c titkos l2-ik1 1.3.6.1.4.1.3955.89.110.1.1.1.0 i 1
snmpset -v2c -c titkos l3-ik2 1.0.8802.1.1.2.1.1.6.1.4.25 x F0
snmpset -v2c -c titkos l3-ik2 1.0.8802.1.1.2.1.1.6.1.4.26 x F0
snmpset -v2c -c titkos l3-ik2 1.0.8802.1.1.2.1.1.6.1.4.27 x F0
snmpset -v2c -c titkos l3-ik2 1.0.8802.1.1.2.1.1.6.1.4.28 x F0
```

lldp lekérdezése:
`snmpwalk -v2c -c public l3-ik2 1.0.8802.1.1.2.1.4.1.1`