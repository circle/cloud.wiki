Magyar előadás diák (UNIX tárgy): http://bagira.iit.bme.hu/~maat/unix/unix_vim_slides/

Angol tutorial: http://benmccormick.org/tag/learning-vim-in-2014/ (lapozz)

### Telepítendők
* https://github.com/tpope/vim-pathogen - pluginekhez
* https://github.com/klen/python-mode - elég sok mindent tud, leginkább az automatikus flake8 futás miatt szeretjük
* https://github.com/ntpeters/vim-better-whitespace - jelöli a sorvégi felesleges szóközöket
* +1 https://github.com/kien/ctrlp.vim - könnyebben lehet fájlok között navigálni




```vim
filetype off
call pathogen#infect()
call pathogen#helptags()
filetype plugin indent on
syntax on

set et
set sw=4
set ai
set smarttab

:nmap <C-k> :tabnext<CR>
:imap <C-k> <Esc>:tabnext<CR>i

:nmap <C-j> :tabprevious<CR>
:imap <C-j> <Esc>:tabprevious<CR>i

autocmd BufRead *.py set textwidth=80
autocmd BufRead *.py set colorcolumn=76
autocmd BufRead *.html,*.js,*.css,*.less set textwidth=0 
autocmd BufRead *.html,*.js,*.css,*.less set sw=2
```