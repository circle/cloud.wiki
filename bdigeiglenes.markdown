nyuszihoz valo dolgok
=====================

.. TODO: Indent lines, set language: Example .. code-block:: python
CELERY_ACKS_LATE = True
CELERYD_PREFETCH_MULTIPLIER = 1


openwrt image készítése
=======================

* http://downloads.openwrt.org/snapshots/trunk/a`revision 71 <changeset/71>`_xx/OpenWrt-ImageBuilder-a`revision 71 <changeset/71>`_xx_generic-for-linux-x86_64.tar.bz2
* kitömörít, belelép
* make image IMAGE=TLWR1043 PACKAGES="-6relayd aiccu base-files busybox curl dnsmasq dropbear dtach firewall freeradius2 freeradius2-utils glib2 ip ip6tables iptables iptables-mod-conntrack-extra iptables-mod-filter iptables-mod-ipopt irssi iw jshn kernel kmod-ath kmod-ath9k kmod-ath9k-common kmod-cfg80211 kmod-crypto-aes kmod-crypto-arc4 kmod-crypto-core kmod-gpio-button-hotplug kmod-ifb kmod-ip6tables kmod-ipt-conntrack kmod-ipt-conntrack-extra kmod-ipt-core kmod-ipt-filter kmod-ipt-ipopt kmod-ipt-nat kmod-ipt-nathelper kmod-iptunnel4 kmod-ipv6 kmod-leds-gpio kmod-ledtrig-default-on kmod-ledtrig-netdev kmod-ledtrig-timer kmod-ledtrig-usbdev kmod-lib-crc-ccitt kmod-lib-textsearch kmod-mac80211 kmod-nls-base kmod-ppp kmod-pppoe kmod-pppox kmod-sched-connmark kmod-sched-core kmod-sit kmod-tun kmod-usb-core kmod-usb-ohci kmod-usb2 libblobmsg-json libc libcurl libdaemon libgcc libip4tc libip6tc libjson libltdl liblzo libncurses libnl-tiny libopenssl libpcap libpthread libreadline librt libstdcpp libubox libubus libuci libusb-compat libusb-1.0 libxtables mtd muninlite nano netifd odhcp6c openntpd openvpn-openssl opkg ppp ppp-mod-pppoe qos-scripts radvd socat strace swconfig tc tcpdump terminfo uboot-envtools ubus ubusd uci usbutils wireless-tools wpad wpad-mini xinetd zlib"
* openwrt-a`revision 71 <changeset/71>`_xx-generic-tl-w`revision 1043 <changeset/1043>`_nd-v1-squashfs-factory.bin