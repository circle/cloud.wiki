= VM-NET =

Infrastruktúra
==============

A jelenlegi topológiában csak a Mega3-Mega4 van közvetlenűl összekötve.


.. TODO: Indent lines, set language: Example .. code-block:: python
                                   ________
[Host - 01]                       [        ]                           .--------.
[Host - 02]--[ Switch / Cross ]---[ Mega3  ]-----[ Router (fw1) ]-----| INTERNET |
[Host - ..]                       `________ <________>`_                           '--------'


== Működése ==

A hálózat a Head node-ot, (jelenleg) a **Mega3**-at használja routerként. 

Libvirt definíciós file
-----------------------

Egy egyszerű NAT-olt definíció az Opennebula 02:00:$IP MAC címekhez a $IP statikusan hozzárendelve.

A hálózat jelenleg a 10.7.5.2-10.7.5.254 intervallumban működik, de ez kiterjeszthető a teljes IP tartományra.

[https://svn.iit.bme.hu/trac/cloud/browser/libvirt/libvirt-network/vm-net.xml vm-net.xml]

Az így létrejött hídhoz, még hozzá kell adni a megfelelő portot(interfacet) amin a többi node elérei.

TODO: Ezt vhogy automatikusan kellene. Libvirt-ben hookal vagy definícióban!


.. TODO: Indent lines, set language: Example .. code-block:: python
sudo ovs-vsctl add-port vm-net eth0


=== Libvirt megkerülése ===
Bonyolultabb hálózat esetén, mint amilyen nekünk is lesz, érdemesebb a bridget+DHCP servert kézzel megcsinálni. A bridge-hez hozzáadjuk az eth0-t, a route-ba beállítunk minden továbbítani kívánt tartományt, hogy a vm-net bridgbe mutasson. 

[https://svn.iit.bme.hu/trac/cloud/browser/libvirt/initscript-nw/vm-nw-init.sh Init-script] az alábbi funkciókhoz.

Bridge:

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo ovs-vsctl add-br vm-net


Külső interface hozzáadása

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo ovs-vsctl add-port vm-net eth0


Route:

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo route add -net 10.7.5.0 netmask 255.255.255.0 dev vm-net


Iptables

.. TODO: Indent lines, set language: Example .. code-block:: python
#Checksum fix (libvirt csinalja biztos kell)
iptables -t mangle -I POSTROUTING -o vm-net -p udp -m udp --dport 68 -j CHECKSUM --checksum-fill

#DHCP és DNS
iptables -t filter -A INPUT -i vm-net -p udp -m udp --dport 53 -j ACCEPT
iptables -t filter -A INPUT -i vm-net -p tcp -m tcp --dport 53 -j ACCEPT
iptables -t filter -A INPUT -i vm-net -p udp -m udp --dport 67 -j ACCEPT
iptables -t filter -A INPUT -i vm-net -p tcp -m tcp --dport 67 -j ACCEPT

#Egymás közti láthatóság + kifele + létrejött kapcsolatok befele agyébként REJECT
iptables -t filter -A FORWARD -d 10.7.5.0/24 -o vm-net -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A FORWARD -s 10.7.5.0/24 -i vm-net -j ACCEPT
iptables -t filter -A FORWARD -i vm-net -o vm-net -j ACCEPT
iptables -t filter -A FORWARD -o vm-net -j REJECT --reject-with icmp-port-unreachable
iptables -t filter -A FORWARD -i vm-net -j REJECT --reject-with icmp-port-unreachable

#Nat fordítás kifele
iptables -t nat -A POSTROUTING -s 10.7.5.0/24 ! -d 10.7.5.0/24 -j MASQUERADE


DHCP:

.. TODO: Indent lines, set language: Example .. code-block:: python
TODO

VLAN Taggelt hálózati csatoló hozzáadása, ezzel lehet ugyanazon a fizikai eszközön több hálózatot kezelni. VLAN=0 ssh+belső, VLAN=1 bme-net

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo ovs-vsctl add-br bme-test vm-net 1


=== Node-ok beállítása ===

TODO: Statikus bridge beállítása, hogy reboot után is működjön.

Mivel a routolást és DHCP szervert is a a fejgép kezeli, a további node-okon elég egy egyszerű bridget létrehozni és a megfelelő interface-hez kötni. És a routolást beállítani.
Bridge:

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo ovs-vsctl add-br vm-net
sudo ovs-vsctl add-port vm-net eth0

Route:

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo route add -net 10.7.5.0 netmask 255.255.255.0 dev vm-net


Portforward
-----------

A mega3 22000 és 23000-es tartományát használja fel.

22<$IP> --> $IP:22 (SSH)

23<$IP> --> $IP:3389 (RDP)

A tűzfalszabályok dinamikusan létrejönnek/törlődnek. Ehhez módosítani kell az oned.conf-ot

.. TODO: Indent lines, set language: Example .. code-block:: python
#*******************************************************************************
# Dynamic iptables rules for administrative network
#*******************************************************************************
VM_HOOK = [
    name = "portfw_add",
    on = "RUNNING",
    command = "portforward.rb",
    arguments = "add $TEMPLATE"
    ]

VM_HOOK = [
    name = "portfw_del",
    on = "DONE",
    command = "portforward.rb",
    arguments = "del $TEMPLATE"
    ]


Valamint el kell helyezni a var/remotes/hooks mappába a `Portforward hook script <https://svn.iit.bme.hu/trac/cloud/browser/opennebula/vnm_hook_pf/portforward.rb>`_-et.

Statikus forward-hoz scriptek:

`Generáló script <https://svn.iit.bme.hu/trac/cloud/browser/libvirt/libvirt-network/generate_iptables.pl>`_

`iptables szabályok <https://svn.iit.bme.hu/trac/cloud/browser/libvirt/libvirt-network/iptables.sh>`_