**********************
OpenVSwitch telepítése
**********************

A hivatalos oldal és a readmek egyszerűek és érthetőek: http://openvswitch.org/

OpenVSwtitch fordítása
======================

Fordítás kernelmodulként:

.. TODO: Indent lines, set language: Example .. code-block:: python
./configure --with-linux=/lib/modules/`uname -r`/build
make
sudo make install


Kernel modulok automatikus betöltése:

.. TODO: Indent lines, set language: Example .. code-block:: python
mkdir /lib/modules/`uname -r`/kernel/net/openvswitch/
cp ./datapath/linux/openvswitch_mod.ko /lib/modules/`uname -r`/kernel/net/openvswitch/
cp ./datapath/linux/brcompat_mod.ko /lib/modules/`uname -r`/kernel/net/openvswitch/
depmod -a


Automatikus indításhoz:

A "bridge" modult le kell tiltani a "/etc/modprobe.d/blacklist.conf" -ban.
Az OpenVSwitch modulokat hozzá kell adni a "/etc/modules" -hez.

.. TODO: Indent lines, set language: Example .. code-block:: python
#OpenVSwitch drivers
  openvswitch_mod
  brcompat_mod


Konfigurálás
============

Az adatbázis létreohzása
------------------------

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo mkdir -p /usr/local/etc/openvswitch
ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema


Indítás
-------

.. TODO: Indent lines, set language: Example .. code-block:: python
ovsdb-server --remote=punix:/usr/local/var/run/openvswitch/db.sock --pidfile --detach

ovs-vsctl --no-wait init #csak 1. inditasnal

ovs-brcompatd --pidfile --detach
ovs-vswitchd --pidfile --detach


Az automatikus indításhoz használjuk az `upstart <http://upstart.ubuntu.com/>`_ configokat.


* [https://svn.iit.bme.hu/trac/cloud/browser/upstart/ovs-dbserver.conf OVS-DatabaseServer]
* [https://svn.iit.bme.hu/trac/cloud/browser/upstart/ovs-brcompat.conf OVS-BridgeCompatMode]
* [https://svn.iit.bme.hu/trac/cloud/browser/upstart/ovs-vswitch.conf OVS-SwitchDaemon]

== Opennebula konfigurálása OVS-hez ==

Engedjük meg az opennebula felhasználónak, hogy használja root-ként az ovs-vsctl programot.

.. TODO: Indent lines, set language: Example .. code-block:: python
visudo



.. TODO: Indent lines, set language: Example .. code-block:: python
...
oneadmin ALL=(ALL)NOPASSWD:/usr/local/bin/ovs-vsctl

