Hallgatói feltételek
====================


Az **IK Cloud** a BME IK és IIT együttműködésében, a VIK támogatásával létrejött rendszer, amelyben az IIT oktatói és hallgatói szükség szerint vehetnek igénybe '''virtuális erőforrásokat'''.

Az **IIT géptermeiben** telepített kliensszoftver segítségével az oktató által előkészített és kiajánlott '''virtuális környezetet a hallgató** a számára megszabott **kvóta keretein belül''' igény szerint indíthatja.

Lehetőség van a gépteremben elkezdett munka otthoni folytatására is a korábban elindított környezetben.
A rendszer segítségével az otthoni feladatot végző hallgató is elkerülheti az összetett szoftverkörnyezet telepítésével járó kellemetlenségeket, figyelmét a szakmai munkára irányíthatja.

A rendszer segítséget nyújt önálló labor, szakdolgozat, diplomaterv vagy tdk-munka készítésénél is: a munkához szükséges virtuális gépeket az önkiszolgáló rendszeren keresztül azonnal használatba lehet venni.

Az elindított virtuális gépekre az adott operációs rendszeren szokásos módon lehet távolról csatlakozni: Windows esetén RDP (távoli asztal), Linux esetén SSH, vagy grafikusan NoMachine NX segítségével. A kapcsolódást a géptermekben telepített kliensszoftver még kényelmesebbé teszi.

Az elindított gépeken a **felhasználói adattár** automatikusan csatolásra kerül. Minden munkát itt érdemes végezni, mivel az a gép leállítása után is elérhető marad.

A felhasználók a rendszert jelen önkiszolgáló felületen keresztül érhetik el, amelybe az egyetemi címtárszolgáltatás (eduID) segítségével léphetnek be. A továbblépéssel elfogadják a következőekben összefoglalt **felhasználási feltételeket**.


* A rendszer nem használható semmilyen jogszabályba, vagy egyetemi '''szabályzatba ütköző tevékenységre'''. http://net.bme.hu/regula/
* **Kizárólag tanulmányi** vagy **kutatási tevékenységhez** használható erőforrás.
* Bár a rendszer használata térítésmentes, a **felesleges gépek leállítása** a felhasználó kötelessége.
* Minden gép indítása '''határidő kijelölésével''' történik. A határidő lejártával '''a gépet figyelmeztetés nélkül megállítjuk'''. Amennyiben a felhasználó a megállítást követően sem kéri meghosszabbítását, a gépet és '''minden rajta lévő adatot törlünk'''!
* A felhasználó felelőssége a futtatott rendszer biztonságos üzemeltetése, a hosszabb időn át futó gépek frissítése.
* A rendszert üzemeltető BME IK lehetőségeihez mérten mindent megtesz a szolgáltatásbiztos üzemért, az egyetemi infrastruktúra sajátosságai miatt előforduló üzemszünetekre azonban számítani kell. A BME IK az üzemeltetés kapcsán a folyamatos üzem sérüléséért vagy adatvesztésért '''minden felelősséget kizár'''. Ezeknek megfelelően szükség esetén a biztonsági mentés és a magas rendelkezésre állású környezet kialakítása a felhasználóra hárul.

Reméljük, hogy a rendszerünk használata megkönnyíti munkájukat, melyhez sok sikert kívánunk!

Oktatói kiegészített változat
=============================



== Hallgatóknak szóló összefoglaló ==