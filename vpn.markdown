belépés
=======

`ssh bd@10.12.1.1` jelszó: ~~nem túl nagy méretű, vagyontárgyak szállítására alkalmas szállítóeszköz~~ már nem, **de** nagyon **jó**.

## vpn kagyló létrehozása


* Ebben a Máté kriptje segít: `./vpn.sh akkounter_nickje`. A létrejövő tar-ban minden szükséges dolog megtalálható.
* /etc/openvpn/ccd/akkounter_nickje fájlba beletenni: `ifconfig-push 10.12.1.123 255.255.255.0`
* Tűzfalban felvenni az ipt akkounter_nickje-vpn formában. (Nem kell semmi rule stb. alapból neki.)
* Sopánkodni, hogy milyen rossz a {tűzfal,vpn,bd,cporta,linux,ubuntu,klaud}

## NetworkManager + OpenVPN + IPv6

```bash
cat >/etc/NetworkManager/dispatcher.d/ikvpn <<'A'
#!/bin/bash
if [ -n "$VPN_IP4_NUM_ADDRESSES" ]
then
	read ip gw <<<"$VPN_IP4_ADDRESS_0"
	IFS=./ read ip1 ip2 ip3 ip4 mask <<<"$ip"
	/sbin/ip -6 addr add 2001:738:2001:4031:12:$ip3:$ip4:0/80 dev $VPN_IP_IFACE
		/sbin/ip -6 ro add default via 2001:738:2001:4031:12:1:1:0 metric 100000
	/sbin/ip -6 ro add 2001:738::/32 via 2001:738:2001:4031:12:1:1:0
fi
A
chmod +x /etc/NetworkManager/dispatcher.d/ikvpn
```
