# Ezmiez? :
A Buildbottal kiválóan lehet automatizálni dolgokat, például fordítást, tesztelést, de igazából akármit (lehet akár pingelni is). Felépítésre master/slave, létrehozunk egy mastert, ez kezel mindent, kvázi ezt kell configolni, illetve kell slave is, ez lehet bárhol, akár másik gépen, másik OS-en, lényeg, hogy tudjon csatlakozni, a buildbothoz, illetve fordítva.

Amúgy a teljes projekt Python, illetve a konfig fájl is rejtetten az (importok, függvényt is lehet akár írni benne)

Honlap: http://buildbot.net :squirrel:

GitHub: https://github.com/buildbot/buildbot :octocat: 

CIRCLE repó: https://git.ik.bme.hu/circle/buildbot :black_circle: 

CIRCLE buildbot: :lock: http://buildbot.vm.ik.bme.hu :unlock:  http://vm.ik.bme.hu:14147

# Főbb részek #

A konfig fájlon végigzongorázva, az alábbi főbb részekkel találkozhatunk

Build Slaves
------------
Itt lehet beállítani őket, kell a nevük és a jelszó hozzájuk. Név != mappa név, jelszó meg igazából default "pass", úgyis csak helyi hálón megy.

Changesources
-------------
Hogyan tudjon a master a forrás kódban levő változásokról, alapból itt volt egy `GitPoller` (X másodpercenként nézi van-e változás), de ez le lett cserélve hook alapú megoldásra, szóva litt nincs semmi!

Schedulers
----------

Ha van egy változás, mi a fenét csináljunk vele, illetve melyik buildert indítsuk (fontos Builder != Build Slave). Itt nekünk van egy `SingleBranchScheduler` (ez ilyen default szerű dolog), ami minden branchre indít, illetve be van állítva, hogy a "miscellaneous" mappában levő változásokat ne figyelje. Illetve van egy `ForceScheduler`, ezzel lehet gombnyomásra indítani buildeket.

Builders
--------

Egy builderhez ezek a főbb infók kellenek 
name: mi legyen a build neve (ez fog megjelenni a honlapon)
slavnames: melyik slaveken fusson, ha többet is megadunk akkor redundáns lesz
factory: ez egy `BuildFactory`-t vár

Egy `BuildFactory` lépésekből áll, ezekből lehet összeállítani a buildet, az első lépés általában lehúzni a Git repót. Utána lehet pl `ShellCommand` lépést hozzáadni, aminek eléggé beszédes neve van, illetve van beépített `PyFlakes` lépés is, ez kvázi egy `ShellCommand` (abból is származik le), csak Waterfall oldalon részletesebb kimenet lesz.

Status targets
--------------

Hol/hogyan jelenjen meg az eredmény. Honlap/e-mail/irc bot. Illetve a git hook-ot is itt lehet beállítani. 

Egyéb
-----

Konfig fájl végén vannak ilyen alap beállítások, pl mi a projekt URL-je, meg neve. Illetve itt kell beállítani, hogy a revisionöket alakítsa linkké (`RevlinkMatch`)

Életre lehelés
==============

Default dolgok egész jók hozzá


```
$ buildbot create-master mappa_neve
$ mv master/master.cfg.sample master/master.cfg
$ buildbot start master/
```

```
$ buildslave create-slave mappa_neve localhost:9989 slave_neve pass
$ buildslave start slave/
```


Lehet 80-as porton is indítani, csak akkor mindig sudo-val kell restartolni, az meg nem jó, ez segít ezen

```
iptables -t nat -A PREROUTING -i eth0 -p tcp --dport 80 -j REDIRECT --to-port 8010
```




Mi nem default
==============

Konfig
------
Alap konfig egész szép, de nem minden jó belőle.

GitPoller helyett hook-os megoldás van (web statusnál kell beállítani)
Misc-ben levő változásokat ne figyeljük, ehhez a Schedulernél a `fileIsImportant` paraméternek kell beadni egy függvényt.
Builderek jók
Status targetnél jó a honlap, esetleg IRC bot, bár az nekem annyira nem tetszett. Illetve kell valami auth is, bár ez nem muszáj (alapból Force Buildhez kell)
Revlink-kel be kell állítani, hogy a Git-es revision hash szép link legyen!

Forráskód
---------

steps/python.py-ban a `PyFlakes` osztály lett átírva, hogy nekünk kedvezőbben színezzen meg gyűjtsön (pl McCabe alapból nem volt benne)


Képek
=====

![Mire jó](http://docs.buildbot.net/current/_images/overview.svg "Mire jó")
![Hogyan működik](http://docs.buildbot.net/current/_images/slaves.svg "Hogyan működik")
![Részletesebben](http://docs.buildbot.net/current/_images/master.svg "Részletesebben")

Parancsok amik kellenek a tesztek futtatásához

```
sudo apt-get install libpq-dev python-dev
sudo apt-get install libmemcached-dev
sudo apt-get install libxml2-dev libxslt1-dev
sudo apt-get install postfix
sudo apt-get install rabbitmq-server
sudo rabbitmqctl delete_user guest
sudo rabbitmqctl add_vhost circle
sudo rabbitmqctl add_user cloud password
sudo rabbitmqctl set_permissions -p circle cloud '.*' '.*' '.*'
```


Telepítés
```
mkvirtualenv buildbot
git clone git@github.com:BME-IK/buildbot.git
cd buildbot/
pip install -e master/
pip install -e slave/
```