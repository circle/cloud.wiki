
= [[span(''Az itt megtalálható scriptek elavultak, a git repóban lévő verzió a legfrissebb.'', style=color: red)]] =

Lásd [`source:cloud/miscellaneous/context <source:cloud/miscellaneous/context>`_]

Hogy ez fusson is induláskor
============================
a `C:\context` könyvtárban el kell helyezni a scriptet, majd: `http://technet.microsoft.com/en-us/library/cc770556.aspx <http://technet.microsoft.com/en-us/library/cc770556.aspx>`_ (vbscriptet kell betallózni)

a `C:\context\logoff.bat`-ot ide `http://technet.microsoft.com/en-us/library/cc770908.aspx <http://technet.microsoft.com/en-us/library/cc770908.aspx>`_ beírni

ez is kellhet: regsetting: HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\Tcpip\Parameters "DisableDHCPMediaSense"=dword:00000001