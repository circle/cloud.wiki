Template létrehozásának feltételei
----------------------------------


* create_template jog (employee@bme.hu csoportnak van alapból)
* privát kvóta a mesterminta-gép indításához (= 1 VM indítása)

Template megosztásának feltételei
---------------------------------

(Template paraméterek: instance_limit per_user_limit)


* megosztó kvótából instance_limit*gépméret

Gép indításának feltételei
--------------------------


* privát kvóta (gépméretnek megfelelően)
* max per_user_limit példány az adott megosztásból a usernek
* instance_limit még van