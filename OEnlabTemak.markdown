Labor
-----
**GPGPU Grid**
Laborban található gépek grafikus kártyáinak összekapcsolása. HTCondor.

**Lokális KVM laborokban**
A cloudban előkészített imagek felhasználása lokális kvm-en.

=== Önlab ===

A cloud egy olyan modell, amely lehetővé teszi konfigurálható számítási erõforrások (hálózat, szerverek, tárolás, szolgáltatások) hálózaton való kényelmes elérését, amit minimális erőfeszítéssel működésbe lehet hozni. A felhő célja, hogy teljesen elfedje a számunkra érdektelen információkat az erőforrásokról.

A BME IK és IIT együttműködésében, a VIK támogatásával hosszabb ideje folyó munka során jött létre az IK Cloud. A rendszer KVM és OpenVSwitch alapokon működik, amelyet a libvirt közvetítésével az OpenNebula cloud manager vezérel. Ezen összetevők köré egy saját fejlesztésű, Django (Python) alapú önkiszolgáló portál épül.

OpenVSwitch QoS
+++++++++++++++

'''Cloud rendszerek hálózati szolgáltatásminőségének biztosítása'''

A hallgatók feladata megismerkedni az OpenVSwitch rendszerrel, valamint a QoS biztosításának módszereivel. A félév végére fel kell állítani egy, az IK Cloudban hasznosítható QoS tesztkörnyezetet, és azon a működést igazoló méréseket kell végezni.

OpenNebula/libvirt/kvm prioritás
++++++++++++++++++++++++++++++++

'''Erőforráselosztás Cloud rendszerekben'''

A hallgató feladata megismerkedni a libvirt és KVM infrastruktúrával, valamint a rövid távú ütemezés módszereivel. A félév végére be kell mutatni egy tesztrendszert, amelyben a virtuális gépekhez prioritási szinteket lehet rendelni, valamint az ennek megfelelő ütemezést mérésekkel kell alátámasztani.

VM monitorozás
++++++++++++++

'''Virtuális gépek monitorozása Cloud környezetben'''

Ha a felhasználók a cloud szolgáltatásainak hatékony igénybevételében közvetlenül (pl. használatarányos számlázás révén) nem érdekeltek, az üzemeltető feladata a fölöslegese erőforrás-használat megakadályozása. Ehhez nagy segítség, ha a működő gépek kihasználtságát és a biztonsági frissítéseit ellenőrizni tudja. A hallgató feladata megismerkedni az elosztott monitorozási megoldásokkal (pl. collectd, munin/rrd). A félév végére be kell mutatni egy kiválasztott megoldás használhatóságát az IK Cloudban futó gépeken.

Hallgatók
---------


* Hálózati QoS `Gál Attila` (IUQ5BL) csehszlovakze@gmail.com


* CPU ütemezés / prioritás `Labányi Oszkár` (Wj9754) o.labanyi@gmail.com


* Monitoring `Gazsi István` (BALAL6) steve.gazsi@gmail.com


* Cporta `Tímár Dávid Patrik` (UCX7CT) t.david92@gmail.com
