
IDE:
====
`Geppetto <https://puppetlabs.com/blog/geppetto-a-puppet-ide/>`_
Szuper kis cucc, GIT és SVN támogatással, igaz lehetne benne completion, de nem találtam hozzá plugint.
Mouseoverre rövid leírást ad az attribútumokról, resourceokról.


Szívások:
=========


== 1.) ==

Puppet kétféle stringet különböztet meg. Szimpla (') és dupla (") aposztrófok közt.
A szimpla aposztróf használata felesleges és zavaró (Gepettoban érdemes bekapcsolni, hogy dobjon warningot/errort, ha használjuk.)
Ugyanis:

user { '${username}':
   ...
}

Létrehozza az ${username} fiókot, ami korlátozott hasznosságú...
---------------------------
user { "${username}":
   ...
}

Létrehozza Pistikét.

A változóbehelyettesítések, regexek mind csak "" között értelmeződnek megfelelően.


== 2.) ==

A file resource path változójában megadott útvonal, nem csak útvonal, hanem a file neve... Elég hülyeség, de ez van.

Magyarázat:
Minden resourcenak van egy title tulajdonsága, ami a deklarációban megadandó. Alapértelmezésben itt ez a file elérési útja, ha az UNDEF.

Példa:
file { 'answertotheultimatequestion.answ':
  ensure => file,

  path => '/root/answers',
}

Létrehozza a '/root-ban az answers nevű filet.
--------------------------------------------------
file { 'answertotheultimatequestion.answ':
  ensure => file,

  path => '/root/answers/answertotheultimatequestion.answ',
}

A kívánt eredményt adja.
--------------------------------------------------
Jó még, de ocsmány, és sokat kell írni hivatkozáskor:

file { '/root/answers/answertotheultimatequestion.answ':
  ensure => file,
}

A kívánt eredményt adja.


== 3.) ==

Puppetban a deklarációnak egyben definíciónak is kell lennie, és fordítva. Önmagában definíció nem állhat. 
(Röviden: Dekiníció vagy deflaráció, ami kíséreties hasonlóságot mutat a deflorációval... Nem összekeverni... Nem összekeverni... :D )

Példa:

$answer_to_the_ultimate_question = 42

$answer_to_the_ultimate_question = 0 // ERROR: multiple assignment

**DE**

if blue is the sky {
  $answer_to_the_ultimate_question = 42
} else {
  $answer_to_the_ultimate_question = 0
}

Jó, mivel a változó, csak egyszer kap értéket.
Természetesen lehet használni a feltételes értékadást is.

Hierarchia
==========

A puppet alapértelmezett hierarchiarendszere:

**puppet**

- **manifests**

   - '''site.pp''' //Ez az a file, amit applyolunk, ide includeoljuk a fancy moduljainkat

- **modules**

   - **fancy_modul**

   - **templates** //ide kerülhetnek *.erb fileok, amikkel rendelelhető a file resourceok tartalma 
   - **files**     //ide kerülhetnek a szerelmeslevelek
   - **manifests** //ide pedig a lényeges kód

   - '''init.pp''' //mindig kell legyen egy ilyen file, ezzel a névvel. Innen találja meg a puppet, mit kell végrehajtani. Ide includoljuk a szükséges *.pp-ket
   - és még sok más *.pp file

   - '''puppet.conf'''''''''
   - és még sok más file, doksiban utána lehet nézni mi mire való


Hasznos linkek tanuláshoz:
==========================
`Puppet learning virtual machine <http://info.puppetlabs.com/download-learning-puppet-VM.html>`_
`Puppet doksi <http://docs.puppetlabs.com/puppet/3/reference/>`_