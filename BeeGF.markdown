# BeeGFS

## Install

## OS Update and Kernel Update

## Checklist

#### Failover
* Check fail-over on network failure

#### Distributed storage
* Check performance
* Check redundancy

#### Redundancy
* Meta store redundancy
* Storage node redundancy

#### Performance
* Overall performance

#### Add new node
* Adding new node administration and rebase overhead