**********************
Accounting / Számlázás
**********************

Az **oneacctd** daemon segítségével történik. Ezt külön el kell indítani az oned mellett.

MySQL
=====
Ahhoz, hogy ne SQLite adatbázist használjon a konfigurációban (acctd.conf) be kell állítani.

.. TODO: Indent lines, set language: Example .. code-block:: python
:DB: mysql://oneadmin:oneadmin@localhost/opennebula

