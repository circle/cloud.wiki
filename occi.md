# Néhány dolog az OCCI szabvánnyal és a CIRCLE-ös OCCI implementációval kapcsolatban

### Az egzotikus Token típus
Valószínüleg egy Enum-hoz hasonló típus, melynek a szabvány szerint nincsenek előre definiált értékei.
A konkrét értékeket az adott OCCI implementáció határozza meg.

Előfordul a Network occi.network.label attribútumánál.
https://www.ogf.org/documents/GFD.224.pdf#Network

### Network occi.network.label attribútum
A 802.1q szabványtól eltérő hálózat szegmentálási megoldásban használt egyedi azonosító.
Pl: VXLAN id
Egyelőre nem használjuk.

### Compute occi.compute.share attribútum értékének kiszámítása
