== Követelmények ==

Az oldal célja, hogy egy egységes tanulmányi portál készüljön az IIT SW csoport számára, ahol az IIT által tanított összes tárgy megtalálható legyen.  A felhasználók a bejelentkezés után elérik az oktatók által védettnek minősített adatokat (pl. előadás fóliákat) és a saját eredményeiket.

A felhasználó képes keresni az összes tárgy nevére, tárgykódjára, és az előadókra is. A keresés eredménye a tárgy avagy az oktató személyes portál profilját adja vissza. A felhasználó szabadon olvashatja a tárgyak nyilvános híreit.

Az oktatók a bejelentkezésük után a saját tárgyuk admin felületét szerkeszthetik. Itt új szöveges híreket, előadás anyagokat tölthetnek fel, melyet jelölhetnek védettnek (csak bejelentkezés után érhető el), illetve nyilvánosnak. Egy egyszerű kezelői felület segítségével felvihetik a hallgatók eredményeit.

Követelmények PDF mellékletként is feltöltve.