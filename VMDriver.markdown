****************
CIRCLE VM Driver
****************
Libvirt python API
==================

/usr/lib/python2.7/dist-packages/libvirt.py

XML guide
=========
Migration
---------

* Disk cache mode must be "none"
* Security label proper configuration

.. TODO: Indent lines, set language: Example .. code-block:: python
 <seclabel type='dynamic' model='apparmor'/>

CPU
---
Network
-------
Configuration to enable KVM to USE /dev/net/tun

* Disable apport on libvirt

/etc/libvirt/qemu.conf

* clear_emulator_capabilities = 0
* user = root
* group = root
* cgroup

.. TODO: Indent lines, set language: Example .. code-block:: python
cgroup_device_acl = [
        "/dev/null", "/dev/full", "/dev/zero",
        "/dev/random", "/dev/urandom",
        "/dev/ptmx", "/dev/kvm", "/dev/kqemu",
        "/dev/rtc", "/dev/hpet", "/dev/net/tun",
    ]

Adding a simple TAP interface without configuration.

.. TODO: Indent lines, set language: Example .. code-block:: python
<interface type='ethernet'>                                 
    <target dev='nw-name'/>                               
    <mac address='02:00:0a:09:01:2d'/>                  
    <model type='virtio'/>                              
    <script path='/bin/true'/>                          
</interface>  

Things to do in NW driver:

* Add to bridge interface
* Add VLAN TAG
* Add flow rules
* Pull up interface
Disk
----
