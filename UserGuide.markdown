********
Útmutató
********

Virtuális gépek
===============
A Cloud IK segítségével lehetőség van előkészített virtuális gépek futtatására. A futtatás akkor lehetséges, ha van elérhető sablon és elegendő kvóta a futtatásához.

Felhasználói adattár
====================

A felhasználói adattár egy állandó adattár a felhő felhasználóinak a részére. Az adattár több módon is elérhető.


* Webes felület
    A webes felületen az Adattár ablakon belül látjuk a a személyes adattárat. Itt lehetőség van a fájlok és mappák letöltésére, feltöltésére átnevezésére.


* Virtuális gép
    A virtuális gép automatikusan csatlakoztatja az adattárunkat. Windows esetén ez lesz a **z:** meghajtó, linux esetén a *home* katalógus **sshfs** mappája (*/home/cloud/sshfs/*)


* Cloud GUI (labor):
    A felhasználói felület automatikusan csatlakoztatja a lokális gép *home* katalógus **sshfs** mappájába.


* Egyéb
    Lehetőség van független sftp (sshfs) kapcsolat kialakítására. Ehhez szükséges egy publikus kulcs hozzáadása a felhasználói beállításoknál.

Virtuális gép mérete
====================
A virtuális gép sablonjaihoz különböző méretek rendelhetőek. Ezek a felhasználható erőforrás mennyiségét jelölik. A különböző méretekhez különböző értékek (kreditek) tartoznak. A kvótákból ez az érték kerül felhasználásra.
|| CPU (Mag) || RAM (MB) || Kredit ||
|| 1 || 1024 || 1 ||
|| 1 || 2048 || 2 ||
|| 2 || 4096 || 4 ||

Kvóták
======

Személyes kvóta
---------------
A személyes kvóta határozza meg, hogy mennyi virtuális gépet **futtathat egyszerre** egy személy. Ebbe csak az ACTIVE státuszú gépek számítanak bele, tehát a szüneteltetett (STOPPED) **nem**.

Megosztó kvóta
--------------
A tanár jogosultságú felhasználók, sablonokat oszthatnak meg a csoportjaikkal. A sablon megosztás egyik feltétele, hogy elegendő megosztó kvóta álljon rendelkezésre. Egy megosztáshoz tartozó kvóta a következőképpen alakul: ''''Gépméret * Példányok maximális száma''''. A megosztó kvótából a megosztáskor kerül levonásra a teljes mennyiség.

Megosztáshoz (csoporthoz) tartozó kvóta
---------------------------------------
Egy sablon megosztásához kétféle kvóta tartozik. Az első a **Példányok maximális száma**. Ez határozza meg, hogy a csoport összesen hány gépet futtathat. A '''Példányok max. száma/felhasználó''' kvóta az egy felhasználó által indítható gépek számát jelenti. Tehát a megosztásban szereplő felhasználó akkor indíthat gépet, ha a csoport még rendelkezik elegendő kvótával és nem lépi túl a felhasználónkénti kvótát sem.

== Határidők ==
Minden létrehozott gép két lejárati idővel rendelkezik. Az egyik a '''Felfüggesztés ideje** a másik a **Törlés ideje'''. Az elindított gép a típusától függően kapja meg ezeket az értékeket. A felfüggesztési idő lejártakor a rendszer a gépet felfüggeszti, a törlési idő lejártakor törli a gépet. A lejárati idők a virtuális gép adatainál meghosszabbíthatóak. 

Sablonok
========
A tanároknak lehetőségük van sablonok létrehozására, amiket megoszthatnak csoportjaikkal. A sablon létrehozása a Sablonok/Új sablon opción keresztül lehetséges. A sablonhoz először egy alaprendszert kell kiválasztani, ami lehet egy beépített sablon vagy akár egy régebben elkészített saját sablon is.
A megfelelő adatok kitöltése után elindul egy **Mesterpéldány** ahova telepíthetőek a szükséges szoftverek. A testreszabás utána a virtuális gép részleteinél a **Mentés** gombra kattintva lehet eltárolni.

Csoportok
=========
A sablonokat csoportokkal lehet megosztani. A rendszer felkínál az oktatott kurzusokhoz egy alapértelmezett csoportot, de lehetőség van saját csoportok kialakítására is.

Megosztás
=========
A sablonokat a megosztásokon keresztül lehet elérhetővé tenni a többi felhasználó számára. A megosztáshoz tartozik a megosztás típusa. Ez határozza meg a gép élettartamát.
|| Típus || Felfüggesztés || Törlés ||
|| Labor || 5 óra || 2 Hét 1 Nap ||
|| Projekt || 1 hónap || 6 hónap ||
|| Szerver || 1 év || - ||