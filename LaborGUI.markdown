# Labor Client


## Telepítés


1. Függőségek telepítése
   ```bash
   apt-get install sshfs sshpass rdesktop python-pip python-webkit
   ```
2. Pip csomag létrehozása
   ```bash
   python setup.py sdist
   ```
3. Csomag telepítése
   ```bash
   sudo pip install dist/CloudGUI-0.3.tar.gz
   ```