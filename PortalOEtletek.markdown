User stories
============

Géza matlabos órát akar tartani az IL408-ban. Otthonról EduID-vel bejelenkezik a portálra, kiválaszt egy Windows7 imaget, rákattint, hogy testreszabná: telepít rá egy Matlabot. Leállítja a gépet, elmenti, és a mátrixbuzulás kurzushoz felveszi mint laborból és otthonról mindenki által egy példányban használható gép.

Józsi bejön a mátrixbuzulás órára, elindítja a kiosk oprendszert, belép EduID-vel, és a mátrixbuzulás órájához ajánlott matlabos gépet választja. A virtuális gép elindul, és RDP-vel bejelentkezik rá. Az óra végén a gépet leállítja, az törlődik. --- Roaming profile jelszavazás nélkül? Kerberos ticket?

Józsi a labor után kipróbálna még egy parancsot a Matlabban, ezért a portálra EduID-vel bejelentkezve elindít egy ilyen gépet, amihez kap egy usert, jelszót és hostot+portot. Erre belépve kipróbálja amit akart, majd leállítja a gépet. Az törlődik.

Géza önlabos hallgatója, Józsi egyszerre három, változatos operációs rendszerű gép között tesztelné elosztott rendszerét a félév során. Géza otthonról belép a portálra, hallgatójának felvesz 3 konkurens gépre kvótát. Józsi otthonról EduID-vel belépve szabadon választott lemezképekkel indíthat egyszerre max. 3 gépet.

== Random ötletek ==

Ha már úgyis kell iskolás modell (oktató-hallgató-kurzus stb.), simán lehetne ez egy newit modul.

Valami kis méretű (tárgyhoz igazodó) storage szolgáltatás is kéne. Ez praktikusan lehet windowson Sambás roaming profile, linuxon nfs home a laborgépek esetében. Otthonról mondjuk sftp vagy webdav.

Megoldás
========

Laborgépeken buta linuxon elindul egy böngésző. Ott eduid bejelentkezés után a portálra jut. Ott a felvett tárgyok alapján ajánl pár imaget, abból választ a hallgató. Ekkor generálunk neki egy jelszót, amit egy rdesktopos/nx-es/ssh-s url-be kódolunk. Ezt a laborgépekben a böngésző majd követni tudja. A kapott jelszóval automatikusan be tud jelentkezni az opennebulával ekkor -- megfelelő contexttel -- elindított vm-re a megfelelő terminálprogrammal. A storage szerveres mappáját smb/sshfs éri el. Ehhez credentialt szintén contextből kap.

https://moqups.com/orymate/mKyvRPX4