https://testcdc.hpc.iit.bme.hu/racktables/index.php?page=rack&rack_id=8

Teljesítmény
============

|       Név       |    Gép    |   CPU   |   Mag   |  Órajel  |         RAM          | Hálózat |
| --------------- | --------- | ------- | ------- | -------- | -------------------- | ------- |
|  cloud (fejgép) | DL180 G6  |  E5649  | 12 + HT | 2.53 GHz |  48 GiB (12 x 4 GiB) |   IB    |
|  mega1          | DL170h G6 |  X5670  | 12 + HT | 2.93 GHz |  64 GiB (16 x 4 GiB) |   IB    |
|  mega2          | DL170h G6 |  X5670  | 12 + HT | 2.93 GHz |  108 GiB (12 x 8 GiB + 2 x 4 GiB + 2 x 2 GiB) |   IB    |
|  mega3          | DL170e G6 |  X5675  | 12 + HT | 3.07 GHz |  104 GiB (12 x 8 GiB + 4 x 2 GiB) |   IB    |
|  mega4          | DL170e G6 |  X5675  | 12 + HT | 3.07 GHz |  64 GiB (16 x 4 GiB) |   IB    |
|  mega5          | DL380 G6       |  E5520  |  8 + HT | 2.27 GHz |  64 GiB (8 x 8 GiB)  |   IB    |
|  mega6          | DL380 G6        |  E5520  |  8 + HT | 2.27 GHz |  64 GiB (16 x 4 GiB) |   IB    |
| összesen      |                        |                  | 76 + HT |                  | 496 GiB                       |            |


Blade szerverek
=============

|       Név       |    Gép    |   CPU   |   Mag   |  Órajel  |         RAM          | Hálózat |
| --------------- | --------- | ------- | ------- | -------- | -------------------- | ------- |
| blade1       | BL460c G1 | E5345        | 8      | 2.33GHz|  16 GiB (8 x 2 GiB)  |     |
| blade15       | BL460c G1 | X5260        | 2      | 3.33GHz|  10 GiB (5 x 2 GiB)  |     |
| blade7       | BL460c G1 | E5345        | 8      | 2.33GHz|  16 GiB (8 x 2 GiB)  |     |
| összesen  |                        |                      |18    |                   |  42 GiB                       |     |


Egyeb
=====

|       Név       |    Gép    |   CPU   |   Mag   |  Órajel  |         RAM          | Hálózat |
| --------------- | --------- | ------- | ------- | -------- | -------------------- | ------- |
| gep 1              | DL160 G6     | E5504    | 4            | 2 GHz       |  4 GiB  (2 x 2 GiB )  | IB          |
| gep 2              | DL320 G6     | E5502    | 2            | 1.86 GHz |  8 GiB  (2 x 4 GiB)   | IB          |
| összesen      |                          |                 | 6            |                     |  12 GiB                       |              | 
