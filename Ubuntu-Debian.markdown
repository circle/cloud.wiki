hpacucli es társainak telepítése:

/etc/apt/sources.list-be:

`deb http://downloads.linux.hp.com/SDR/downloads/ProLiantSupportPack/Ubuntu oneiric current/non-free` valamelyik észlény úgy gondolta, hogy jó vicc az ubuntut Ubuntura cserélni...


kulcs letöltése:

`wget http://downloads.linux.hp.com/SDR/downloads/ProLiantSupportPack/GPG-KEY-ProLiantSupportPack -O - | apt-key add -`


hpacucli használata:


.. TODO: Indent lines, set language: Example .. code-block:: python
setarch x86_64 --uname-2.6 /usr/sbin/hpacucli

hpacucli  ctrl slot=3 ld all show detail // cloud-on igy kell megnezni a diszkeket
hpacucli  ctrl slot=3 pd all show detail // cloud-on igy kell megnezni a diszkeket 
ctrl all show config detail

ctrl sn=P92CB0BMQRO02Q modify drivewritecache=enable
ctrl sn=P92CB0BMQRO02Q physicaldrive 1:10 modify led=on
diszkcsere utan:
rescan
ctrl sn=P92CB0BMQRO02Q ld 4 modify led=on modify reenable forced 



.. TODO: Indent lines, set language: Example .. code-block:: python
Linux - hpacucli

This document is a quick cheat sheet on how to use the hpacucli utility to add, delete, identify and repair logical and physical disks on the Smart array 5i plus controller, the server that these commands were tested on was a HP DL380 G3 server with a Smart Array 5i plus controller with 6 x 72GB hot swappable disks, the server had Oracle Enterprise Linux (OEL) installed.

After a fresh install of Linux I downloaded the file hpacucli-8.50-6.0.noarch.rpm (5MB), you may want to download the latest version from HP. Then install using the standard rpm command.

I am not going to list all the commands but here are the most common ones I have used thus far, this document may be updated as I use the utility more.

Utility Keyword abbreviations
Abbreviations	chassisname = ch
controller = ctrl 
logicaldrive = ld
physicaldrive = pd 
drivewritecache = dwc
hpacucli utility
hpacucli	# hpacucli 

# hpacucli help

Note: you can use the hpacucli command in a script
Controller Commands
Display (detailed)	hpacucli> ctrl all show config
hpacucli> ctrl all show config detail
Status	hpacucli> ctrl all show status
Cache	hpacucli> ctrl slot=0 modify dwc=disable
hpacucli> ctrl slot=0 modify dwc=enable
Rescan	hpacucli> rescan

Note: detects newly added devices since the last rescan
Physical Drive Commands
Display (detailed)	hpacucli> ctrl slot=0 pd all show
hpacucli> ctrl slot=0 pd 2:3 show detail

Note: you can obtain the slot number by displaying the controller configuration (see above)
Status	
hpacucli> ctrl slot=0 pd all show status
hpacucli> ctrl slot=0 pd 2:3 show status

Erase	hpacucli> ctrl slot=0 pd 2:3 modify erase
Blink disk LED	hpacucli> ctrl slot=0 pd 2:3 modify led=on
hpacucli> ctrl slot=0 pd 2:3 modify led=off
Logical Drive Commands
Display (detailed)	hpacucli> ctrl slot=0 ld all show `detail <detail>`_
hpacucli> ctrl slot=0 ld 4 show `detail <detail>`_ 
Status	hpacucli> ctrl slot=0 ld all show status 
hpacucli> ctrl slot=0 ld 4 show status
Blink disk LED	hpacucli> ctrl slot=0 ld 4 modify led=on
hpacucli> ctrl slot=0 ld 4 modify led=off
re-enabling failed drive	hpacucli> ctrl slot=0 ld 4 modify reenable forced 
Create	# logical drive - one disk 
hpacucli> ctrl slot=0 create type=ld drives=1:12 raid=0 

# logical drive - mirrored 
hpacucli> ctrl slot=0 create type=ld drives=1:13,1:14 size=300 raid=1

# logical drive - raid 5 
hpacucli> ctrl slot=0 create type=ld drives=1:13,1:14,1:15,1:16,1:17 raid=5

Note:
drives - specific drives, all drives or unassigned drives
size - size of the logical drive in MB
raid - type of raid 0, 1 , 1+0 and 5
Remove	hpacucli> ctrl slot=0 ld 4 delete
Expanding	hpacucli> ctrl slot=0 ld 4 add drives=2:3
Extending	hpacucli> ctrl slot=0 ld 4 modify size=500 forced
Spare	        hpacucli> ctrl slot=0 array all add spares=1:5,1:7

