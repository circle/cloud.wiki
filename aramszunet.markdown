Erről kell írni:

* szervizkapcsoló
* megfelelő kábelek (nagy apc-hez)
* lekapcsolt a fejgép, mert apcd
* infiniband (ki kell húzni utána)
* visszakapcsolás után nem volt a generátoros aljzatokban feszültség
* klíma (úgy általában)
* kamerarendszer üzemelt?
* beléptetőrendszer(legyen nálad kulcs)
* ipari aljzat nem dízeles(a közelében lévők sem, csak az ablaknál)
* FELKÉSZÜLÉS(lekapcsolás, áramellátás ellenőrzése, mi hova van dugva)
* mindenki legyen beosztva egy szerverszobához (ha lehet)
* 412 és iit-s kulcs

Mi történt: 

* bd elaludt, Máté úton
* 8:13 lekapcsolták(mielőtt mega5-6 le lett kapcsolva)
* észleltük, hogy az ipari aljzatok nem dízelesek, nincs klíma sem
* KT szünetmentesre dugta a másodikon lévő `újraindult <újraindult>`_ gépeket (cronos barátai), és a cloud rack 2u-s APC-jét dízelesre (2200-ashoz nem volt kéznél kábel).
* 8:44 Máté->bd, Máté mondta, hogy mi a helyzet, bd mondta, hogy hol van kábel, és az az SFS-nek sürgős.
* Máté bedugta az sfs/2200-ast.
* 8:51 bd->Máté, bd szólt, hogy nincs cronos [közben elindult].
* 9:00 Máté->bd, Máté azt gondolta, hogy a 2u-s APC-re van dugva a fejgép, bejelentette, hogy a harmadikon a Tamás kihúzta véltetlenül az sfs szünetmentes elosztóját
* 9:04 Máté->bd, Máté szólt, hogy kész

Kontaktok: 

* Mázik Gábor üzemeltetésvezető-helyettes (villany, klíma) Kraft FM Kft. 1/463-4215, 30/416-4583
* I épület porta 1/463-1200
* Ketler Tamás 30/232-8277