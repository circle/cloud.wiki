== tűzfalban lévő modellek ==
Rule
----
Egy szabályt ír le, tartozhat tűzfalhoz(firewall), hosthoz(host), csoporthoz(hostgroup), vlanhoz(vlan), vlangrouphoz(vlangroup), de egyszerre csak egyhez.

* direction: megadja, hogy melyik irányra vonatkozik az adott szabály: '1': az adott host/vlan/firewall kapja a csomagot, '0': az adott host/vlan/firewall küldi a csomagot
* description: az adott szabály mire jó
* foreign_network: az a vlangroup, ahonnan csomagot akar fogadni, illetve küldeni az adott host/vlan/stb.
* dport: célport `fontos: ha nat=True, akkor ez a külső port; ha nat=False, akkor ez a külső és belső port is`
* sport: forrásport
* proto: protokoll(tcp,udp,icmp)
* extra: iptables ezeket a paramétereket kapja meg a fentieken túl
* accept: a csomagot elfogadja-e vagy sem
* owner: a szabály tulajdonosa
* r_type: a szabály típusa, ez csak az adminfelületen való megjelenítést szolgálja, máshol nincs használva
* nat: ha ez be van pipálva, akkor létrehoz egy portforwardot a megadott paraméterekkel(külső port: dport, belső port: nat_dport)
* nat_dport: ha a nat be van pipálva, akkor ez lesz a belső port
* vlan, vlangroup, host, hostgroup, firewall: megadja, hogy az adott szabály mihez tartozik, egyszerre csak egyhez tartozhat

Vlan
----

* name: vlan neve, pl: off, dmz, war
* vid: vlanid
* interface: az interfész neve a tűzfalban, pl: vlan0007
* ipv4: a tűzfal címe az adott vlanban
* net4: a használt ipv4 hálózat prefixhossz nélkül
* prefix4:
* ipv6: a tűzfal címe az adott vlanban
* net6: a használt ipv6 hálózat prefixhossz nélkül
* prefix6:
* snat_ip: megadja, hogy milyen ip-re kell átírnia a csomag forráscímét, ha olyan vlan felé küldünk, ami szerepel a snat_to-ban
* snat_to: megadja, hogy melyik vlanok felé küldött csomag forráscímét kell átírni
* domain: az adott vlanon használt gépek domaincíme, az adott gép nevéből(hostname) és a vlan domainnevéből áll össze a gép fqdn-je.
* reverse: 
* dhcp_pool: ha azoknak a gépeknek is akarunk adni dhcp-n keresztül ip-t, amik nincsenek regisztrálva, akkor be kell írni a használandó tartomány első és utolsó címét

VlanGroup
---------

* name: group neve
* vlans: az adott VlanGroup-ba tartozó vlanok

Group
-----
Gépek csoportja, ezen keresztül használjuk fel újra a már meglévő szabályokat. Pl: webszerver csoport, ez két szabályt tartalmaz, 80-as és a 443-as portot engedélyezi

* name: csoport neve, pl: webszerver

Host
----

* hostname: 
* reverse: ha üres akkor a hostname+domainbol jön
* mac:
* ipv4:
* pub_ipv4:
* shared_ip: ha be van pipálva, akkor több gép osztozik egy ip-n, tehát a pub_ipv4-nek nem kell egyedinek lennie
* vlan: az a vlan, amiben a gép van
* groups: ezekben a csoportokban van benne a gép

Record
------

* name: a név a domain nélkül. Ha pl. a felho.cloud.ik.bme.hu esetén felhő
* domain: az a domain, amibe a rekord tartozik
* host: a rekord tartozhat egy hosthoz. Ha ki van töltve, akkor A, AAAA, esetén nem szabad kitölteni se a name-t, se az address-t, ezeket automatikusan kitölti. CNAME esetén pedig csak a name-t kell kitölteni
* address: az az ip vagy domainnév, ahova a rekord mutat. '''A, AAAA, CNAME esetén üresnek kell lennie, ha a host ki van töltve'''
* type: a rekord típusa


== Gyakori műveletek ==

== Szabályok, DNS, DHCP generálása ==

== Konfigok újratöltése ==

Redundancia
===========

fw2: 152.66.243.145

fw2b: 152.66.243.146

=== működés ===

* mindkét gépen fut a keepalived démon, a **man** vlan-on figyelik egymást
* ha kihullik az fw2, akkor a `/root/ha.sh up` parancs meghívódik fw2b-n, ami

   * elindítja az összes vlan-t (pub, man kivételével)
   * pub-ra felhúzza az összes ipv4 (natolt, dmz hálózatok megfelelő címeit), ipv6 (2001:738:2001:4030::fffe-t, amire a 2001:738:2001:4031::/64 routolva van) címet 

* ha megjavul, az fw2, akkor:

   * fw2-n lefut `/root/ha.sh up`
   * fw2b-n lefut `/root/ha.sh down`

* az ixion viszonylag ritkán arpzik, így a művelet géptől függően pár perc kieséssel jár.
