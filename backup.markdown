# Gépek mentése
A mentésre a `duplicity`-t használjuk.

A mentést általában a /root/root-backup.sh script végzi, ami be van jegyezve a root crontab-jába.

## Portálok mentése
A /root/root-backup.sh script először dump-olja az adatbázist a /root/circle_dump.sql fájlba.
Majd létrehoz a `root` LV-ról egy snapshot-ot, felcsatolja /snap/<LV_neve> könyvtárba, majd tartalmát archiválja a backup szerverre.

## Visszaállítás
1. Hozz létre egy snapshot-ot a root LV-ről!
2. Csatold fel a snapshot-ot!
3. Törölj a snapshot-ról mindent, kivéve az exclude-olt könyvtárakat!
4. Állítsd vissza duplicity-vel a kívánt állapotot a snapshot-ra!
5. Ha van selinux akkor a snapshot gyökerébe tégy egy `.autorelabel` nevű fájlt! (Ez rendbe rakja a selinux label-öket.)
6. Csatold le a snapshot-ot!
7. Merge-eld a snapshot-ot és indítsd újra a gépet!
8. Állítsd vissza az adatbázist az SQL dump-ból!
9. Teszteld a rendszert és old meg az esetlegesen felmerülő problémákat!

# Diszk kiesés
Ha kiesik egy diszk, akkor a következőket kell tenni:
* hibás diszket ki-be kell húzni
* setarch x86_64 --uname-2.6 /usr/sbin/hpacucli
* rescan
* megkeresni a hibásat: ctrl all show config detail 
* ctrl sn=P92CB0BMQRO02P ld 5 modify reenable forced
* mdadm --remove /dev/md9 /dev/cciss/c2d16
* mdadm --add /dev/md9 /dev/cciss/c2d16
* cat /proc/mdstat kimenetét megnézni