
* A kód minden része angol nyelvű:

   * Az azonosítók
   * A kommentek
   * A stringek, a felületre kerülők `_()`-ben (template-ben `{% trans "szöveg" %}`-be vagy blocktransba).

* **OsztályokNeve** angol nyelvű, TeveBetűs, egyes számban.
* **metódusok_neve** és **tagváltozók_neve** kisbetűs, angol nyelvű, szavak között `_`.
* Az indentálás mindenhol egységesen '''4 szóköz''', kivéve HTML, Javascript és LESS fájlokban, ott csak 2!
* A fájlok UTF-8 kódolásúak, unix formátumúak (minden sor végén LF).
* Bináris operátorok mindkét oldalán szóköz. (Kivétel: neves paraméterek megadásánál.)
* Vessző után mindig szóköz. Komment elején szintén szóköz (jó: `# asd`, rossz: `#asd`).
* Zárójel a zárójelezett kifejezéshez tapad.
* Sor végén nincs fölösleges whitespace.
* Egy sor maximum 76 karakter (puha limit).
* Egy sor maximum 85 karakter (kemény limit).
* A commit-üzenetek elején "%s: " formában szerepeljen a hackolt modul neve.
* if/while stb. után amit nem kell zárójelbe rakni, azt nem rakjuk. (jó: `if asd:`, rossz: `if(asd):`)
* Ha szintaktikailag hibás kódot commitol valaki, akkor söradási kötelezettsége keletkezik a többiek felé. [wiki/SyntaxError Bővebben]


* Html kódban az önlezáró tageket is jelöljük (`<br />`).
* CSS-ben egy beállítás egy sor, a selector után szóközzel jön a kapcsos zárójel. (`body {` `    color: pink;` `}`)
* JS-ben, sor végén is van `;`, kivéve ha `}`-re végződik. Egy utasítás lehetőleg egy sor.


* URL-ek végén minden esetben van `/`.
* Ahol ez műszakilag kivitelezhető, az url bejegyzés a app/urls.py-ben szerepel, a modul neve alá include-olva.
* Ahol lehet, a django-féle url-feloldást (`get_absolute_url`, `reverse`, `{%url%}`) használjuk.


* Aktívan használjunk naplózást. A lényegesebb (~kb. minden 10. sor kód) műveletekről legalább debug szintű bejegyzés készüljön. Ehhez kell: fájl elején `import logging` `logger = logging.getLogger(__name__)`; a történés helyére pl. `logger.debug("`django <django>`_`one <one>`_ delete orphan host %s", host)`.
