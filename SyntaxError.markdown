
* Ha a hibádat te javítod ki, akkor mindenkinek kell adnod egy sört.
* Ha a hibádat más javítja ki, akkor a két sörrel jössz.
* Ha valakivel kölcsönösen tartoztok egymásnak, akkor ez nem csökkenti a tartozásodat.
* Az így szerzett sör nem használható fel újra.
