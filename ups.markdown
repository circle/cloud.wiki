L212:

rack alján nagy 2200-as: 
* cloud (ide van kötve a menedzsmentje és apcupsd  be van állítva, hogy ne kapcsolja le)
* mega3
* mega4
* infiniband (nincs redundánsan bekötve, ha azonos fázissal egyszerre visszajön az áram, nem bootol be)
* hp switch

rack tetején 2 u-s 1000-es: (nincs bekötve a menedzsmentje, mert amikor próbáltam, akkor lekapcsolt a soros kábel bedugásakor)
* mega1
* mega2
* mega5

L312:

2200-as ups a rackben:
* fw2 (ide van kötve a menedzsmentje)

2200-as ups a sarkoban (menedzsmentje nincs bekötve):
* video
* testcdc
* l3-ik1, l3-ik2, l3-ik3, l3-iit1, l3-iit2, l3-iit3, l3-iit4, linksys buta, médiakonverter, mit switch, 2 ap

posta ups (fehér 700-as vagy 1000-es)
* posta (nincs menedzsment bekötve, lehet nincs is)

t1 ups (fekete 700-as vagy 1000-es)
* t1 (nincs menedzsment bekötve, nyomtatókábel kell hozzá)

backup ups (fekete 700-as vagy 1000-es)
* backup (nincs menedzsment bekötve, nyomtatókábel kell hozzá)