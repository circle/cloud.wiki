**********************
OpenVSwitch használata
**********************

VLAN tag
--------

Amennyiben VLAN taggelt forgalmat használunk, gondoskodjunk róla, hogy a fizikai interface-n kijusson az 1504 byte méretű csomag is!

STP engedélyezése hidakon
-------------------------

.. TODO: Indent lines, set language: Example .. code-block:: python
/usr/local/bin/ovs-vsctl set Bridge b`revision 0 <changeset/0>`_ stp_enable=true

