Ez volt a menete:

- megnövelni a felületen
- fdisk /dev/vda
  - kilistázni a partíciókat (l)
  - törölni az extended partíciót (d, 2)
  - csinálni egy extendedet, leenterezni
  - csinálni egy logikait, ugyanolyan kezdettel, mint előzőleg volt
  - átállítani a típusát (t, 8e)
  - menteni (w)
- újraolvastatni a partíciós táblát: partprobe  -s /dev/vda
- lvm fizikai kötet extendelése: pvextend /dev/vda5
- lvm logikai kötet extendelése: lvextend -L+30G /dev/ubuntu-vg/root
- fájlrendszer kiterjesztése: resize2fs /dev/ubuntu-vg/root