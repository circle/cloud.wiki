Mit kell migrálni

A jelenlegi south migrációk nem használhatók közvetlenül, mivel pár el lett rontva és információvesztés lép fel.

firewall:
* Vlan - nincs sok módosítás
* Host - nem volt sok módosítás
* Rule - pár field át lett nevezve, megcserélve, pl dport és nat_dport
* Record - ha létezik hozzácsatolt host, akkor már nem viselkedik speciálisan (eddig ilyenkor bizonyos adatokat más modelből szedett, miközben a field üres volt)
* Blacklist - nincs módosítás
* Domain - nincs módosítás
* Group - nincs módosítás
* VlanGroup - nincs módosítás
* Firewall - nincs módosítás

felhasználók, csoportok:
* Person, UserCloudDetails helyett Profile lett, pár átnevezett field
* school.{group,semester,course} nem lesz migrálva

one:
* template: csak azt migráljuk, amelyből fut vm
* instance: futó és alvó gépek migrációja
* share: nem lesznek migrálva
* disk, network: közvetlenül nem lesznek migrálva

virtuális gépek:
* kell írni egy scriptet, ami minden opennebulás virtuális gépről összegyűjti az alábbiakat:
** milyen diszkeket használ és ha van, akkor annak mi a base imidzse (opennebula, qemu-img) és létrehozni az új model szerint
** milyen hálózatot használ (opennebula) és ezeket létrehozni az új model szerint
** ram, cpu (opennebula)
** tulajdonos, név, lejárat (django)

* mi legyen az opennebulában lévő, circleben nem lévő gépekkel?

### Levél oktatóknak

Tárgy: IK cloud migrálása (reakció szükséges)

---

Kedves Gipsz Jakabné prof. dr. PHp!


Ezt a levelet azért kapja, mert jelenleg használja a BME IK és az IIT által üzemeltetett cloud rendszert.

A rendszer jelentős változás előtt áll: az IK-ban is üzembe helyezzük a működtetés során szerzett tapasztalatok alapján új alapokra helyezett fejlesztésünket, a CIRCLE Cloud-ot. Ezzel egyidejűleg a jelenlegi rendszeren futó virtuális gépeket átvezetjük az új rendszerbe, és a régi rendszert véglegesen leállítjuk.

A migrálást három hét múlva, várhatóan 2014. augusztus 2-3-án végezzük. Ennek keretében a  futó gépeket leálltjuk, majd a sikeres migrálást követően már az új rendszerben indítjuk el. A gépek leállítását szabványos módon ("kikapcsológomb" ACPI signal) végezzük, azonban aki szeretné, szombat hajnalig maga is leállíthatja (például a virtuális gépen végrehajtott shutdown paranccsal). Az új rendszerben a gépek minden fontos jellemzője változatlan marad. Azokat a gépeket fogjuk újból elindítani, amelyek augusztus 1-jén 0 órakor futottak.

A sablonokat abban az esetben tartjuk meg, ha belőlük a migrálás pillanatban virtuálisgép-példány fut. Amennyiben olyan sablont kíván a továbbiakban is használni, amelyből egy példány sem fut, azt a levél végén szereplő űrlapon jelezheti.

HA a migrálás ma történne, a következő sablonokat tartanánk meg:
  * xxx
és a következőket TÖRÖLNÉNK:
  * yyy

Az új rendszerben a csoportok és a jogosultságok kezelése dinamikusabbá válik, ezzel szükségtelenné téve egyes csoportokat, megosztásokat. Ezt mérlegelve a csoportok és megosztások migrálásától eltekintünk, azonban ha van olyan csoportja, amelyet a továbbiakban is meg kíván tartani, azt jelzés esetén áthozzuk.

Az új rendszer előnyeinek sorolása helyett bátran javasoljuk, hogy próbálja ki a https://demo.cloud.bme.hu/ címen (az itt indított gépek és sablonok rövid időn belül törlésre kerülnek, a rendszer itt csak bemutató célból működik), vagy tekintse meg a rendszer honlapját: http://circlecloud.org/

Várhatóan szeptember 4-én csütörtökön délután egy rövid ismertetőt tartunk a rendszerről minden érdeklődő, új és régi felhasználó számára.


KÉRJÜK, a zökkenőmentes migrálás érdekében töltse ki a következő űrlapot:
zzz

Amennyiben nem kér több levelet a migrálásról, vagy adatainak törlését kéri, azt is a fenti űrlapon jelezheti.

Üdvözlettel: az IK CIRCLE Cloud fejlesztői és üzemeltetői csapata


* user guide?










