Alap dolgok:
============


* **sokáig támogatott disztró**t kellene használni (pl.: centos)
* **csak csomagból** telepíthető komponenseket szabad használni és lehetőleg hivatalos repóból
* mindez azért kell, mert ennek a rendszernek '''hosszú ideig kell majd működnie'''
* lehetőleg kerülni kell a php-s hekkelések használatát

Mindenképpen kell:
==================

* '''stabil imap, stmp szerver'''
* **webmail** (csomagból)
* legyenek **virtuális felhasználók** (pl. mysqlben)
* egy felületen be lehessen állítani a '''szűrési szabályok'''at
* felhasználók kezelésére '''adminfelület'''
* vírus-, spamszűrés
* dkim meg hasonlók támogatása