***************
Windows 7 Guest
***************

Passwordless bejelentkezés
==========================
Felhasználó konfigurációja/Felügyeleti Sablonok/Rendszer/ctrl+alt+del beállításai jelszó módosítása gomb eltávolítása
Felhasználó konfigurációja/Felügyeleti Sablonok/Rendszer/Energiagazdálkodás

Speciális config
================

* Felhasználónév: User
* Autotune kikapcsolása (RDP idle ne szakadjon meg)
  

.. TODO: Indent lines, set language: Example .. code-block:: python
netsh interface tcp set global autotuning=disabled


* Kiterjesztések elrejtése 
* Cache törlés
* Notepad++
* UTC RTC


Telepítés
=========
A Windows telepítése opennebulából **nem** lehetséges, mivel (nem tudom miért) csak 1 DB CD-ROM-ot lehet hozzáadni egy templatehez.

A telepítéshez szükséges a quest-driver cd, különben sosem fog merevlemezt találni a telepítő.

`Fedora által karbantartott driverek <http://alt.fedoraproject.org/pub/alt/virtio-win/latest/images/bin/>`_

=== Előkészület ===

* 1 db Legalább 16GB de inkább 30GB-s virtuális lemez
* 1 db Windows 7 telepítő CD
* 1 db Guest driver CD


.. TODO: Indent lines, set language: Example .. code-block:: python
kvm-img create -f raw 30G



A telepítést folytathatjuk kvm-es parancssorból vagy libvirt deployment fileből létrehozott virtuális gépen. Fontos hogy már a telepítésnél legyen az **ACPI** support 'engedélyezve'.

Ha elindult kapcsolódunk VNC-vel, a merevlemez választásnál telepítjük a quest drivert és kész.

Save As / Shutdown
==================

Ezekhez a funkciókhoz ACPI support kell. '''Fontos a Windows ACPI szignálokra (mint Shutdown) csak akkor reagál, ha a konzolon (nem RDP-n, hanem a KVM-es VNC konzolon) be van jelentkezve.'''

RDP
===

AZ RDP-hez kell 1 jelszóval rendelkező felhasználó, valamint engedélyezni kell a !Számítógép/Tulajdonságok/Távoli elérés menüpontban.

Context
=======
`scriptek <wiki/windows7context>`_

~~cloud user jelszavát átállítani~~ kész

ha minden kész, CD-t kiadni!
Store
=====

~~Samba fut a store.local címen. kéne valami olyasmi, hogy `net use z: "\\store.local\NEPTUN" /User NEPTUN smbjelszó /persistent`~~ kész

~~Meg utána kéne nézni, h lehet-e (power)shellből a dokumentumokat átrakni ide.~~ kész

Asztalra z: meghajtóra parancsikont.

virtio utolag
=============

virtio driverek telepitese utolag:


#. bootoljunk be windows 7 telepitorol, shift+f10-zel hozzuk elo a cmd-t
#. virsh attach-disk <vmneve> </utvonal/a/virtiocdhez> hdc --type cdrom --mode readonly
#. adjuk ki ezt: dism /Image:C:\ /Add-driver /Driver:E:\win7\x86 /recurse

Csökkentett/Recovery boot menü letiltása
========================================

Adjuk ki Admin-nal a következő parancsot:
`bcdedit /set {current} bootstatuspolicy ignoreallfailures`