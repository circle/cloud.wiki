== Jelenleg használatban lévő zellerek (`celery`) ==


* lokális zeller és celerybeat: django@cloud user alatt fut: `./manage.py celery worker --loglevel=info -c 1 -Q local -B` '''EZ FUSSON DEVENVBEN IS! '''
* opennebula zellere `celery -A opennebula_celery worker --loglevel=info -c 1 -Q opennebula` '''EZ FUSSON DEVENVBEN IS! ''' (/opt/webadmin/cloud/miscellaneous/celery-ben legyünk)
* dns zeller: tinydns@monitor user alatt (django@cloud-rol lehet `ssh dns`) fut: `.local/bin/celery -A dns worker --loglevel=info -c 1 -Q dns`
* fw, dhcp zeller: fw@fw2 user alatt (django@cloud-rol lehet `ssh fw2`) fut: `.local/bin/celery -A fw worker --loglevel=info -c 1 -Q dhcp,firewall`


A **monitor**on es a **fw2**-n a megfelelő python csomagok **pip**pel vannak telepítve **lokálisan**!

rabbitmq
========


* csomag felrakása

  `apt-get install rabbitmq-server`


* guest user törlése

  `rabbitmqctl delete_user guest`


* új vhost hozzáadása

  `rabbitmqctl add_vhost django`


* új user hozzáadása

  `rabbitmqctl add_user django jelszó`


* adsz neki jogot a vhost-ra

  `rabbitmqctl set_permissions -p django django '.*' '.*' '.*'`


* megnézed, hogy minden jól ment-e

  `rabbitmqctl list_permissions -p django`

  ilyesmit kell kapnod:

.. TODO: Indent lines, set language: Example .. code-block:: python
  Listing permissions in vhost "django" ...
  django .* .* .*
  ...done.


Lásd még: [https://giccero.cloud.ik.bme.hu/trac/cloud/browser/cloud/miscellaneous/devenv/nextinit.sh#L62]

== vm-ek állapotának frissítése ==
'''/etc/one/oned.conf'''-ba:

.. TODO: Indent lines, set language: Example .. code-block:: python
VM_HOOK = [                                                                 
    name      = "django",                                                   
    on        = "CREATE",                                                   
    command   = "/opt/update_state",                                        
    arguments = "$ID"                                                       
]                                                                           
VM_HOOK = [                                                                 
    name      = "django",                                                   
    on        = "RUNNING",                                                  
    command   = "/opt/update_state",                                        
    arguments = "$ID"                                                       
]                                                                           
VM_HOOK = [                                                                 
    name      = "django",                                                   
    on        = "SHUTDOWN",                                                 
    command   = "/opt/update_state",                                        
    arguments = "$ID"                                                       
]                                                                           
VM_HOOK = [                                                                 
    name      = "django",                                                   
    on        = "STOP",                                                     
    command   = "/opt/update_state",                                        
    arguments = "$ID"                                                       
]                                                                           
VM_HOOK = [                                                                 
    name      = "django",                                                   
    on        = "DONE",                                                     
    command   = "/opt/update_state",                                        
    arguments = "$ID"                                                       
]                                                                           
VM_HOOK = [                                                                 
    name      = "django",                                                   
    on        = "FAILED",                                                   
    command   = "/opt/update_state",                                        
    arguments = "$ID"                                                       
]
VM_HOOK = [                                                                 
    name      = "django",                                                   
    on        = "UNKNOWN",                                                   
    command   = "/opt/update_state",                                        
    arguments = "$ID UNKNOWN"                                                       
]


`sudo /etc/init.d/opennebula restart`

`ln -s /opt/webadmin/cloud/miscellaneous/celery/opennebula_celery.py /opt/update_state` '''EZ MEGVÁLTOZOTT! '''