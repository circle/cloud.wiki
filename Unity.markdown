Unity launcher stickelt cuccok:

settings set com.canonical.Unity.Launcher favorites "['chromium-browser.desktop', 'firefox.desktop', 'libreoffice-writer.desktop', 'libreoffice-calc.desktop', 'libreoffice-impress.desktop', 'cloud.desktop', 'modvalto.desktop']"

Ez ebbe ír bele:

/home/labor/.config/dconf/user

desktop fájl helye:

/usr/share/applications




Autostart:

lokális: /home/labor/.config/autostart/modvalto.desktop

globális: /etc/xdg/autostart/nvidia-autostart.desktop

Módváltó desktop fájl:

.. TODO: Indent lines, set language: Example .. code-block:: python

`Entry <Desktop>`_
Name=Képernyő módváltás
Exec=modvalto
Icon=preferences-desktop-display
Terminal=false
Type=Application
Categories=Utility;
X-Desktop-File-Install-Version=0.20



Módváltó szkript:

.. TODO: Indent lines, set language: Example .. code-block:: python

xrandr --output DVI-D-0 --primary

if xrandr|grep -qs "HDMI-0 connected"
then
	:
else
	zenity --info --text "Nincs a tévé csatlakoztatva."
	exit 1
fi


ans=$(zenity --height=300 --list  --text "Válasszon módot." --radiolist  --column "" --column "Beállítás" \
TRUE "Tükrözött" \
FALSE "Kiterjesztett 1080p" \
FALSE "Kiterjesztett 720p" \
FALSE "Csak tévé 720p" \
FALSE "Csak tévé 1080p" \
FALSE "Csak monitor")

xrandr --output HDMI-0 --off
xrandr --output DVI-D-0 --mode 1280x1024
case "$ans" in
Tükrözött)
	xrandr --output DVI-D-0 --mode 1280x1024
	xrandr --output HDMI-0 --same-as DVI-D-0
	xrandr --output HDMI-0 --mode 1280x720
;;
Kiterjesztett\ 1080p)
	xrandr --output DVI-D-0 --mode 1280x1024
	xrandr --output HDMI-0 --mode 1920x1080
	xrandr --output HDMI-0 --left-of DVI-D-0
	xrandr --output DVI-D-0 --primary
;;
Kiterjesztett\ 720p)
	xrandr --output DVI-D-0 --mode 1280x1024
	xrandr --output HDMI-0 --mode 1280x720
	xrandr --output HDMI-0 --left-of DVI-D-0
	xrandr --output DVI-D-0 --primary
;;
Csak\ tévé\ 720p)
	xrandr --output HDMI-0 --mode 1280x720
	xrandr --output DVI-D-0 --off
;;
Csak\ tévé\ 1080p)
	xrandr --output HDMI-0 --mode 1920x1080
	xrandr --output DVI-D-0 --off
;;
Csak\ monitor)
	:

esac

killall compiz
compiz --replace &
disown %1



