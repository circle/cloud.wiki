CIRCLE projektre vonatkozó fejlesztési információk:
-------

A fejlesztést a DevEnv (**Dev**elopment **Env**iroment) fejlesztői környezeteken végezzük. Ennek részletes leírása a [fejlesztői környezet](DevEnv) wikin olvasható. 

Célszerű egyedi, személyre szabott környezetet használni, akár projektenként is. Ennek két megközelítése lehetséges.

1. **Egyedi létrehozása saját rendszeren**. Egy teljesen új DevEnv a CIRCLE csoportban lévő [cloud](https://git.ik.bme.hu/circle/cloud) repository és a különböző driver repositorykban lévő telepítési leírások követésével hozható létre.
> [Összes CIRCLE repository](https://git.ik.bme.hu/groups/circle) 

2. **[BME CIRCLE Cloudon](https://cloud.bme.hu/) futó előre elkészített DevEnv sablonok használata**. Ezek feltelepítve és beállítva tartalmazzák már az összes komponenst, és így önállóan működnek valódi virtuális gépekkel.


A jelenlegi fejlesztési elvárás a stable master policy-t követi. Tehát a masterben csak működő és minden szempontból megfelelő kód kerülhet be. Ezt több lépésben biztosítjuk.
Jelenlegi fejlesztési cél: A jelenlegi rendszer karbantartása és kiegészítése.
>Elfogadható megoldásnak csak a [kódolási](CodingStyle) és [tesztelési](Teszteles) elvárásoknak megfelelő kódokat tekintjük.