= Általános Linux tudnivalók virtuális gépen =

Save As / Shutdown
==================
Szükséges az ACPID csomag telepítése. Érdemes az "/etc/acpi/powerbtn.sh"-t lecserélni, egy sima "shutdown -h now" -ra. És akkor nem lesz olyan, hogy vagy leáll vagy nem.

Context
=======

Context fájlok
--------------

[http://opennebula.org/documentation:rel3.2:cong Opennebula context how-to]

`scriptek <wiki/ubuntucontext>`_

=== context beüzemelése ===


* '''libpam-mount, sshfs''' csomag kell hozzá
* **/etc/context** könyvtárba bemásoljuk a scripteket
* megbizonyosodunk róla, hogy **/etc/context/firstrun** létezik
* '''/etc/rc.local'''-hoz hozzáadjuk: `/etc/context/init.sh`
* '''/etc/context/cleanup.conf''' bemásolása /etc/init alá




