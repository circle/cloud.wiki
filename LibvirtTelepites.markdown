******************
Libvirt telepítése
******************

== Csomagfüggőségek telepítése ==

.. TODO: Indent lines, set language: Example .. code-block:: python
apt-get install libxml2-dev gnutls-dev libdevmapper-dev python-dev libsasl2-dev libnl-dev


Fordítás
========

.. TODO: Indent lines, set language: Example .. code-block:: python
./configure
make
make install
ldconfig


Konfigurálás
============
Ahhoz hogy a libvirt létre tudja hozni a bridgeket nem szabad a default dnsmasq programnak elindúlni. 

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo update-rc.d dnsmasq disable


Libvirt konfigurálása Opennebulához
===================================
A libvirt használatához elérhetővé kell tenni a kommunikációs socketet a "libvrtd.conf" -ban.

Ez alapból a "/usr/local/etc/libvirt/libvirtd.conf" helyen található.

.. TODO: Indent lines, set language: Example .. code-block:: python
unix_sock_group = "cloud"	
unix_sock_ro_perms = "0777"
unix_sock_rw_perms = "0770"
auth_unix_ro = "none"
auth_unix_rw = "none"


A "/usr/local/etc/libvirt/qemu.conf"-ban be kell állítani a megfelelő user és group-ot amin keresztűl a VM-ek futnak.

.. TODO: Indent lines, set language: Example .. code-block:: python
# The user ID for QEMU processes run by the system instance.
user = "oneadmin"

# The group ID for QEMU processes run by the system instance.
group = "cloud"

# Whether libvirt should dynamically change file ownership
# to match the configured user/group above. Defaults to 1.
# Set to 0 to disable file ownership changes.
# Ergo ne változtasson az image jogosultságain!
dynamic_ownership = 0


Ebben az esetben az oneadmin user-t hozzá kell adnunk a kvm csoporthoz, hogy elérhesse a /dev/kvm drivereket.

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo adduser oneadmin kvm

