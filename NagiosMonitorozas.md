* OPS
https://ops.ik.bme.hu/

* Hőmérő
https://stats.ik.bme.hu/temp/1d

* Graphite
https://graphite.ik.bme.hu/

* csomagok telepítése:
 `apt-get install nagios-nrpe-server nagios-plugins-basic nagios-plugins-standard`

* konfigolás
 ```
sed -i -e 's/^allowed_hosts.*/allowed_hosts=127.0.0.1,10.2.0.9,10.3.3.4,152.66.243.60/' -e '$a command[check_linux_raid]=/usr/lib/nagios/plugins/check_linux_raid' -e 's/dont_blame_nrpe=0/dont_blame_nrpe=1/' -e '/check_disk/ s/^#//' /etc/nagios/nrpe.cfg
```

* nrpe szerver restartolása:
 `/etc/init.d/nagios-nrpe-server restart`


* A következő pluginok mehetnek ekkor az OPS-be:

   * OS - Base Unix Agent/Disk
   * OS - Base Unix Agent/Unix Load Average


Ezzel kompatibilis tűzfalbeállítások:

```
ufw allow http   # < tényleges szolgáltatások
ufw allow from 10.0.0.0/8 to any port 5666 proto tcp
ufw allow from 152.66.243.0/24 to any port 5666 proto tcp
ufw allow from 10.0.0.0/8 to any port 22 proto tcp
ufw allow from 152.66.243.0/24 to any port 22 proto tcp
ufw allow from 2001:738:2001:4030::/60 to any port 22 proto tcp
ufw enable
```

\+ a klaudban monitorozható csoport
