Ezen az oldalon soroljuk fel a fél napokat elvett szívásokat – megoldással.

----
A **host** gépen blacklist ellenére '''betöltődik a bridge modul'''.
> Valószínű, hogy a libvirt demon-ban maradt default network, és amikor a virb`revision 0 <changeset/0>`_-t hozzáadja a brctl betölti a bridge modult.
----
Reboot után nem megy az openvswitch (pl frissült a kernel)
> `rmmod bridge; dpkg-reconfigure openvswitch-datapath-dkms && /etc/init.d/openvswitch-switch restart && /etc/rc.local`
----
VM indításánál error: "libvirt Unable to add bridge cloud port vnet0: No such process"
> Nem fut a brcompat demon. (Nem elég a kernelmodul betöltése) /etc/defaults/openvswitch-switch
----
A **NOPASSWD sudo** nem működik és a user benne van az admin/sudoer csoportban.
> ''When multiple entries match for a user, they are applied in order. Where there are multiple matches, the last match is used (which is not necessarily the most specific match).''
>
> Tehát több illeszkedő sornál az '''utolsó illeszkedő sor''' lesz az érvényes!
> -- Sanyi 2013-01-30

----
Nem importál semmit a django '''manage.py loaddata'''.
> Nem létezik a paraméterként kapott fájl. (Nincs hibaüzenet.) -- Máté 2013-01-31

----

Nem fut le a *-commit git hook.
> Csak kliensoldalon működik. *-receive kell neked. -- Máté 2013-02-01

----

Néha a modeldict nem talál egy kulcsot. Nekünk manage.py collectstatic után jött elő, és apache reload után tűnt el.
> modeldict/models.py setdefault függvény végére:
> `return value # FIXED maat Mon Feb  4 15:21:33 CET 2013`

----

Upstarttal nem működik valami pythonos cucc. Akkor bezzeg megy, ha kézzel indítom.
> Nincs locale upstartból. Megoldás: `changeset:0f29ce0/cloud <changeset:0f29ce0/cloud>`_. Máté 2013-02-04

----
Git panaszkodik: `fatal: Unable to write new index file`
> Elfogyott a hely, jobb ötlet híján restart megoldja.

----
Webkit becachelt valami marhaságot és nem jól jelenimk meg az oldal
> rm -rf /home/%u/.local/share/webkit/* törli a cachet

----
A tinydns bizonyos mennyiségű query után timeout-ot dob.
> a multilog nem tudja írni a könyvtárát, `chown -R Gdnslog:Gdnslog /etc/tinydns/log` megszereli.

----
A Django minden statikus fájlra 404 errort dob.
> A Django kikapcsolt debug módban nem hajlandó a statikus fájlokat kiszolgálni (ez fícsör), mert "az az Apache dolga". Ha mégis kellenének a fájlok, akkor `--insecure` kapcsolóval kell indítani a szerver (`./manage.py runserver` satöbbi)

----

`git push` működik, de nem lép ki a végén.
> Elakad egy (szerver-oldali) hook, pl. mert nem fut az ii 

----

Nem indul a rabbitmq-server.
> Nem működik számmal kezdődő hosztnévvel.

----

Celery workeren belüli subprocess-es hívás hatására 13-as hibakóddal kilép a worker.
> Ez egy ismert billiard bug, 2.7.3.24-es verzióban került javításra.

----

A celery nem csatlakozik vissza a rabbitmq szerverre és olyanokat ír, hogy: `ConnectionError: Too many heartbeats missed`
> Szerintem kombu bug, ami állítólag a 2.5.11 javítva lett.

----

A celeryn keresztül beküldött kérésre nem jön válasz (timeout), de látszólag nem ír senki semmi hibát.
> Mindkét oldalon a CELERY_TASK_RESULT_EXPIRES = 3600 legyen beállítva.

----

A django változatos módon panaszkodik a stringekre.
> Lehet azért van, mert nem így hoztad létre a db-t: `CREATE DATABASE <dbname> CHARACTER SET utf8;`


### Nem működnek az időzített taskok
Ha a slowcelery a `DBPageNotFoundError`-ral panaszkodik, akkor a törölni kell a circle/circle/celerybeat-schedule fájlt, majd újraindítani a slowcelery-t!