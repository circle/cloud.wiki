****************
Rendszer indulás
****************

Opennebula
==========

#. OpenVSwitch

   #. ovs-dbserver (upstart)
   #. ovs-brcompatd (upstart)
   #. ovs-vswitchd (upstart)

#. Libvirtd (upstart)
#. Iptables-persistent (init.d)
#. Network init (init.d)

   #. vm-net (ifconfig + dhcp)
   #. bme-net (ifconfig + dhcp)

#. Opennebula (init.d)

Node
====

#. OpenVSwitch

   #. ovs-dbserver (upstart)
   #. ovs-brcompatd (upstart)
   #. ovs-vswitchd

#. Libvirtd (upstart)