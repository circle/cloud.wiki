Így kell beröffenteni:

az url-t cseréld le a sajátodra, de a / jel maradjon a végén, mert különben vicces hibákat kapsz! 

```bash
cat >> ~/.virtualenvs/circle/bin/postactivate <<END
export DJANGO_SAML=TRUE
export DJANGO_URL=https://demo.cloud.bme.hu/
export DJANGO_SAML_ATTRIBUTE_MAPPING='{"mail": ["email"], "sn": ["last_name"], "eduPersonPrincipalName": ["username"], "givenName": ["first_name"]}'
export DJANGO_SAML_GROUP_OWNER_ATTRIBUTES='niifEduPersonHeldCourse'
export DJANGO_SAML_GROUP_ATTRIBUTES='eduPersonScopedAffiliation,niifEduPersonAttendedCourse'
export DJANGO_SAML_ORG_ID_ATTRIBUTE='niifPersonOrgID'

END

sudo apt-get install xmlsec1:i386  # ha 64 bitet telepítesz, akkor egy idő után segfaultok lehetnek
cp -rp ~/.virtualenvs/circle/lib/python2.7/site-packages/saml2/attributemaps/ ~/circle/circle/attribute-maps
wget --no-check-certificate https://idp.ik.bme.hu/idp/profile/Metadata/SAML -O ~/circle/circle/remote_metadata.xml
sudo cat /etc/ssl/private/ssl-cert-snakeoil.key >~/circle/circle/samlcert.key
cp /etc/ssl/certs/ssl-cert-snakeoil.pem ~/circle/circle/samlcert.pem
```
***

A `~/circle/circle/attribute-maps/saml_uri.py` fájlt szerkesszük át ennek megfelelően:
 * fro részbe: ```
        'urn:oid:1.3.6.1.4.1.11914.0.1.154': 'niifPersonOrgID',
        'urn:oid:1.3.6.1.4.1.11914.0.1.164': 'niifEduPersonAttendedCourse',
        'urn:oid:1.3.6.1.4.1.11914.0.1.166': 'niifEduPersonHeldCourse', ```
 * to részbe: ```
        'niifPersonOrgID': 'urn:oid:1.3.6.1.4.1.11914.0.1.154',
        'niifEduPersonAttendedCourse': 'urn:oid:1.3.6.1.4.1.11914.0.1.164',
        'niifEduPersonHeldCourse': 'urn:oid:1.3.6.1.4.1.11914.0.1.166', ```