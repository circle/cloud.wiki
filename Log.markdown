
== Röviden ==

Logolásra a django beépített log rendszerét használjuk. Következőkben megpróbálom röviden összefoglalni, mit, miért úgy valósítottam meg, ahogy, s hogy hogyan lehet használni a log keretrendszert.

A log keretrendszer a következő elemeket biztosítja számunkra: log hívások, loggerek, handlerek, filterek s formatterek. Ezek közül az első az, amit a kódban elhelyezve az üzenetet ír a logba, ha meghívódik. Utóbbiak pedig ezen üzenetek feldolgozását leíró szabályok. Az alábbiakban ezekről kívánok röviden írni. A django logolási lehetőségeiről bővebben: https://docs.djangoproject.com/en/1.5/topics/logging/

Log hívások
===========

Az alábbi log hívásokat különböztetjük meg, prioritás szerint a legerősebbtől a leggyengébb felé:

* logger.critical()
* logger.error()
* logger.warning()
* logger.info()
* logger.debug()

A kódban a log hívások előtt szerepelnie kell(ene) a következő két sornak:

.. TODO: Indent lines, set language: Example .. code-block:: python
import logging
logger = logging.getLogger(__name__)

A logging.getLogger paramétere string, amely nevet a loggernek szánjuk. Amennyiben itt meghagyjuk a {{{__name__ }}} változót, a logger neve a django app nevével lesz azonos. Ez lehetőséget ad arra, hogy egy django appon belül két loggerünk legyen, s az azokon keresztül indított log hívásokat másképp dolgozzuk fel.

.. TODO: Indent lines, set language: Example .. code-block:: python
import logging

logge`revision 1 <changeset/1>`_ = logging.getLogger('app_log1')
logge`revision 2 <changeset/2>`_ = logging.getLogger('app_log2')

logge`revision 1 <changeset/1>`_.debug('Ez most a log1-en keresztül')
logge`revision 2 <changeset/2>`_.debug('ez pedig a log2-n keresztül megy')


Log beállítások
===============

Egy kicsit kipuculva lejjebb látható egy példa log beállítás, alatta rövid magyarázat.


.. TODO: Indent lines, set language: Example .. code-block:: python
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters':{
        'simple': {
            'format': '%(asctime)s [%(levelname)s]: %(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
    },
    'filters': {
    },
    'handlers': {
        'syslog': {
            'level': 'DEBUG',
            'class': 'logging.handlers.SysLogHandler',
            'formatter': 'simple',
            'address': LOGSERVER, # LOGSERVER = ("logs.papertrailapp.com", 14526)
        },
        'logfile': {
            'level':'INFO',
            'class':'logging.handlers.RotatingFileHandler',
            'filename':'/opt/webadmin/cloud/logfile.log',
            'maxBytes': 1024,
            'backupCount': 5,
            'formatter': 'simple'
        },

    },
    'loggers': {
        'one':{
            'handlers': ['syslog'],
            'level': 'DEBUG',
        },
    }
}



Szóval:

* Megmondja, hogy a beállítást a ’dictConfig version 1’ formátumban írták. (Jelenleg más verzió nincs, kompatibilitási okokból szerepel)
* Az összes eddigi log beállítást letiltja.
* Nos, itt következik a formatterek leírása

   * Melyből jelen példában egy van, s ’simple’ névre hallgat. A ’format’ sorban leírom az üzenet megjelenését a logban (jelen példában: aktuális_idő `szintje <log>`_: üzenet), alatta pedig az idő megjelenését.

* Majd jöhetne a filterek leírása. A filterek segítségével még tovább szűrhetnénk, hogy a loggerektől mely üzenetek jussanak el a handlerekig. Alapbeállításnál a django egy filtert ad, melyről bővebben [[https://docs.djangoproject.com/en/dev/releases/1.4/#request-exceptions-are-now-always-logged|itt]] olvashatsz (röviden: "kompatibilitási okok" miatt van).
* S definiálok két handlert:

   * Egyet syslog néven, mely a simple formattert használja, s minden neki átadott, Debug szintű, vagy a feletti logot kezel. A Python [[http://docs.python.org/2/library/logging.handlers.html#sysloghandler|SyslogHandler]] osztályát használja. A LOGSERVER változó a settings fájl elején definiált, amint a megjegyzésben meg van adva. Jelenleg ez még a papertrailapp.com 3rdParty szolgáltató felé küldi a logokat, ezt kell majd átírni a belso syslog szerverre
   * S egyet logfile néven, mely szintén a simple formattert használja, s Info szintű, vagy a feletti logokkal foglalkozik. A Python által biztosított [[http://docs.python.org/2/library/logging.handlers.html#rotatingfilehandler|RotatingFileHandler]] osztályt használja. Ez a handler a 'filename' sorban megadott fájlba írja a logokat, amíg a fájl el nem éri a 'maxBytes' sorban megadott méretet. Amennyiben a logfájl az új loggal meghaladná a maxBytes méretet, akkor a log üzenet új fájlba lesz írva. A régebbi logfájlok a backupCount sorban szereplő szám erejéig meg lesznek tartva logfile.log.1, logfile.log.2, ... néven. Új logfájl kezdésekor a régi logfile.log-ból lesz logfile.log.1, a logfile.log.1-ből logfile.log.2, és így tovább, míg a "legöregebb" logfájlt törli a rendszer. Amennyiben a maxBytes sor nulla, egy sosem lesz új logfájl kezdve. Fájlba írt logok esetén érdemes megnézni még a [[http://docs.python.org/2/library/logging.handlers.html#filehandler|FileHandler]] s a [[http://docs.python.org/2/library/logging.handlers.html#timedrotatingfilehandler|TimeRotatingFileHandler]] Python osztályokat.

* Végül, de nem utolsó sorban, pedig a loggerek. Ebből csak egy áll fent példának, mégpedig:[`BR <BR>`_]Ami a one nevű loggerel hívott log üzeneteket a syslog handler felé továbbítja, amennyiben az eléri a DEBUG szintet. Amennyiben több handler felé szeretnénk továbbítani egy-egy üzenetet, akkor a szögletes zárójelen belül, vesszővel elválasztva kell felsorolni a handlereket.


Megjegyzés
==========
https://papertrailapp.com/ - csak hogy ne felejtődjön el, online log management