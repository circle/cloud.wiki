```
apt-get install syslog-ng

cat > /etc/syslog-ng/conf.d/03remote.log << LOGOLNIAKAROK
destination d_remote {
 udp("10.2.0.1" port(514));
};
log { source(s_src); destination(d_remote); };
LOGOLNIAKAROK

/etc/init.d/syslog-ng restart
```