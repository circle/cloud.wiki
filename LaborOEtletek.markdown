
* Erre alkalmas sablonok futhatnak helyi kvm-en is.
* CG csoportnak vga vagy "csak" gpgpu kell? Ha utóbbi, elég lehet kiajánlani?
* Éjszaka/hétvégén gpu-grid alkalmas zöld ki-bekapcsoló ütemezővel?
* Éjszaka/hétvégén labor image távolról használható?
* Helyben futtatott image diffje igény esetén visszatölthető, és otthonról elindítható?
 
 