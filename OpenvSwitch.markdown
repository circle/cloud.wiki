acl
===

=== amit az OpenNebula csinál ==
az OpenNebula feltesz alapból pár aclt:


.. TODO: Indent lines, set language: Example .. code-block:: python
sudo /usr/bin/ovs-ofctl add-flow cloud in_port=336,dl_src=02:00:0a:09:02:e8,priority=40000,actions=normal
sudo /usr/bin/ovs-ofctl add-flow cloud in_port=336,priority=39000,actions=drop


de ez nem elég, mivel csak mac-re filterez.

javítás a helyzeten
-------------------
ezt megcsinálva javul a helyzet: (tudom, így nem a legjobb)


.. TODO: Indent lines, set language: Example .. code-block:: python
--- /home/cloud/OpenvSwitch.rb	2013-05-22 20:36:19.629651002 +0200
+++ OpenvSwitch.rb	2013-05-22 22:17:21.685651001 +0200
@@ -35,7 +35,11 @@
             tag_vlan if @nic`:vlan <:vlan>`_ == "YES"
 
             # Prevent Mac-spoofing
-            mac_spoofing
+            if ['vm-net', 'war'].include?(@nic`:NETWORK <:NETWORK>`_)
+                ip_spoofing
+            else
+                mac_spoofing
+            end
 
             # Apply Firewall
             configure_fw if FIREWALL_PARAMS & @nic.keys != []
@@ -70,6 +74,19 @@
         add_flow("in_port=#{port}",:drop,39000)
     end
 
+    def ip_spoofing
+        ipv4 = @nic`:ip <:ip>`_
+        d = ipv4.split('.')
+        ipv6 = "2001:738:2001:4031:#{d`revision 1 <changeset/1>`_}:#{d`revision 2 <changeset/2>`_}:#{d`revision 3 <changeset/3>`_}:0/112"
+
+        add_flow("in_port=#{port},dl_src=#{@nic`:mac <:mac>`_},udp,tp_dst=68",:drop,43000)
+        add_flow("in_port=#{port},dl_src=#{@nic`:mac <:mac>`_},ip,nw_src=#{ipv4}",:normal,42000)
+        add_flow("in_port=#{port},dl_src=#{@nic`:mac <:mac>`_},ipv6,ipv6_src=#{ipv6}",:normal,42000)
+        add_flow("in_port=#{port},dl_src=#{@nic`:mac <:mac>`_},arp,nw_src=#{ipv4}",:normal,41000)
+        add_flow("in_port=#{port},dl_src=#{@nic`:mac <:mac>`_},udp,tp_dst=67",:normal,40000)
+        add_flow("in_port=#{port}",:drop,39000)
+    end
+
     def configure_fw
         # TCP
         if range = @nic`:black_ports_tcp <:black_ports_tcp>`_


ilyeneket kell látnunk a logban:

.. TODO: Indent lines, set language: Example .. code-block:: python
"sudo /usr/bin/ovs-ofctl add-flow cloud in_port=14,dl_src=02:00:c0:a8:01:0d,udp,tp_dst=68,priority=43000,actions=drop"
"sudo /usr/bin/ovs-ofctl add-flow cloud in_port=14,dl_src=02:00:c0:a8:01:0d,ip,nw_src=192.168.1.13,priority=42000,actions=normal"
"sudo /usr/bin/ovs-ofctl add-flow cloud in_port=14,dl_src=02:00:c0:a8:01:0d,ipv6,ipv6_src=2001:738:2001:4031:168:1:13:0/112,priority=42000,actions=normal"
"sudo /usr/bin/ovs-ofctl add-flow cloud in_port=14,dl_src=02:00:c0:a8:01:0d,arp,nw_src=192.168.1.13,priority=41000,actions=normal"
"sudo /usr/bin/ovs-ofctl add-flow cloud in_port=14,dl_src=02:00:c0:a8:01:0d,udp,tp_dst=67,priority=40000,actions=normal"
"sudo /usr/bin/ovs-ofctl add-flow cloud in_port=14,priority=39000,actions=drop"

