****************
Guest telepítése
****************
== Előkészületek / Template ==
DATABLOCK
---------
A friss rendszerhez üres tárterület kell amit az EMPTY DATABLOCK opcióval lehet beállítani az imageben. Új rendszer telepítéséhez érdemes a DATABLOCK partíciónak az FS TYPE mezőjébe a **raw** vagy  **qcow2** formátumot megadni, ekkor egy formázatlan merevlemez képfájl készül. A DATABLOCK-ot minden esetben **virtio** driverrel használjuk és ennek megfelelően '''vd*'''' (vda,vdb,...) elérési úttal (TARGET)

=== CD-ROM ===
A telepítő CD minden esetben **ide** driverrel használjuk és az ennek megfelelő '''hd*''' (hda,hdb,...) elérési úttal (TARGET).
'''Legyen perzisztens! '''

Hálózat
-------
Alap telepítésnél elég a '''vm-net''' predefined hálózatot további beállítások nélkül hozáadni.

Telepítés folyamata
===================
Legegyszerűbb, ha az aktuális HOST megfelelő portjára kapcsolódunk egy VNC viewer-rel. A telepítés a szokványos módon végezhető.

Telepítés vége
==============
A telepítés befejeztével a virtuális gép template file-ből '''távolítsuk el a CD-ROM** részt és a boot opciót állítsuk vissza **hd'''-re
