# What's this about:
A(n) ~~easy~~ way to tell you how to ~~properly get to~~ run selenium tests on your current devenv.

|Are you intrested about|You can read about it here|
|---------------------|----------------------------------------|
|What is Selenium|http://www.seleniumhq.org/docs/01_introducing_selenium.jsp|
|Selenium Python binding|https://selenium-python.readthedocs.org/|


> **Important note:**
> At the writing of this page the commonly used DevEnv is an Ubuntu 14.04 so, the easy links are for those ones. Also the current Selenium version is 2.45.0 which requires a Firefox 35.0 to run properly.

>|Programs|Links to check out|
>|--------|------------------|
>|Newest Selenium versions|http://www.seleniumhq.org/download/|
>|Firefox builds for Ubuntu (.deb)|https://launchpad.net/~ubuntu-mozilla-security/+archive/ubuntu/ppa|

Let's get started
------------
First we need to install **Firefox** on our DevEnv.
```
sudo apt-get update
sudo apt-get install firefox
```
> **Important note:**
> Here comes the tricky part usually Selenium is behind the latest realeses and doesn't work well with the new ones. To get this problem out of the way we need to downgrade our install.
```
wget https://launchpad.net/~ubuntu-mozilla-security/+archive/ubuntu/ppa/+build/6457763/+files/firefox_33.0%2Bbuild2-0ubuntu0.14.04.1_amd64.deb
sudo dpkg -i firefox_33.0+build2-0ubuntu0.14.04.1_amd64.deb 
```
> Also because of unattended-upgrades this will be replaced with the newest stable version. Let's put firefox to the unattended-upgrades blacklist. To do that open the 
```
/etc/apt/apt.conf.d/50unattended-upgrades
```
file and edit the 
```
Unattended-Upgrade::Package-Blacklist
```
list to include "firefox" like this:
```
Unattended-Upgrade::Package-Blacklist {
		"firefox";
}
```
Finally save the modifications.

Next we need to install a (virtual) **X display server**. The current fabfile uses the **Xvfb** so I personally recommend that one.
> More about Xfvb: http://en.wikipedia.org/wiki/Xvfb

```
sudo apt-get install Xvfb
```

Check the Python packages
------------
Do not forget to check if the required test packages are installed
```
pip install -r requirements/test.txt
```

It's done
------------
That's it. Now you can run the selenium test just as you would run the normal tests.

Namely:

```
fab selenium
```
or
```
xvfb-run --server-args="-screen 0, 1920x1080x24" ./manage.py test --settings=circle.settings.selenium_test
```
>You can run one test alone, but in selenium **it's not recommended**. Usually each test requires previous tests successfull run to work (test) on with.
>```
fab selenium:dashboard.tests.selenium.basic_tests:BasicSeleniumTests.test_01_login
```

View test logs and controll pictures
------------
To view the log simply tail or open the following files:
```
tail -f -n 100 'dashboard/tests/selenium/selenium.log'
```

>For convinience Selenium tests takes pictures in some cases of exceptions, to help see the problem visually. These are
```
find dashboard/tests/selenium -type f -name "*.png"
```
Namely

>|Exception|Picture name|
>|------------|------------|
>|Cannot list the select possibilities|ss_from_list_options.png|
>|Cannot select the chosen one|ss_from_select_option.png|
>|Cannot find the requested href|ss_from_get_link_by_href.png|
>|Cannot inject javascript to the page|ss_from_click_on_link.png|
>|Cannot find the form controls|ss_from_login.png|
>|Cannot accept the operation confirmation|ss_from_wait_and_accept_operation.png|
>|Cannot save a vm as a template|ss_from_save_template_from_vm.png|
>|Cannot create a base template virtual machine|ss_from_create_base_template.png|
>|Cannot find the template's id|ss_from_get_template_id.png|
>|Cannot check the result of an operation|ss_from_check_operation_result.png|
>|Cannot filter timeline activities to find most recent|ss_from_recently.png|
>|Selenium could not locate the timeline|ss_from_get_timeline_elements.png|
>|Cannot get timeline elements|ss_from_get_timeline_elements.png|
>|Selenium did not found any activity|ss_from_get_timeline_elements.png|
>|Cannot find the searched activity|ss_from_get_timeline_elements.png|
>|Cannot start a template from a base one|ss_from_create_template_from_base.png|
>|Cannot delete the desired template|ss_from_delete_template.png|
>|Cannot start a VM|ss_from_create_random_vm.png|
>|Cannot destroy a VM|ss_from_delete_vm.png|