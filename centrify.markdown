Gyakori feladatok
=================
`ssh user@ik-dc.cloud.ik.bme.hu`

jelszóváltoztatás
=================
`adpasswd`

felhasználó hozzáadása
----------------------

.. TODO: Indent lines, set language: Example .. code-block:: python
sudo -i samba-tool user add bd --given-name="Bach Dániel" --mail-address="bd@ik.bme.hu"
sudo -i samba-tool group addmembers linuxshell bd
sudo -i samba-tool group addmembers linuxadmin bd





Tartományba léptetés
====================
=== szükséges beállítások ===


* /etc/hostname `c-1117`
* /etc/hosts:

.. TODO: Indent lines, set language: Example .. code-block:: python
127.0.0.1       localhost
127.0.1.1       c-1117 c-1117.ik.local



* /etc/network/interfaces:

.. TODO: Indent lines, set language: Example .. code-block:: python
    dns-nameservers 10.2.1.1
    dns-search ik.local #nem muszáj


centrify telepítése
-------------------

.. TODO: Indent lines, set language: Example .. code-block:: python
echo "deb http://archive.canonical.com/ precise partner" > /etc/apt/sources.list.d/partner.list
apt-get update
apt-get install centrifydc


=== domain-be léptetés ===
`adjoin -w ik.local --force`

=== kerberos-os bejelentkezés engedélyezése ===
/etc/centrifydc/centrifydc.conf végére:

.. TODO: Indent lines, set language: Example .. code-block:: python
pam.allow.groups: linuxshell

/etc/ssh/sshd_config végére:

.. TODO: Indent lines, set language: Example .. code-block:: python
GSSAPIAuthentication yes
GSSAPIKeyExchange yes
GSSAPICleanupCredentials yes
PasswordAuthentication no
AllowGroups linuxshell

/etc/sudoers végére:

.. TODO: Indent lines, set language: Example .. code-block:: python
%linuxadmin ALL=(ALL) ALL


reboot
------
vagy `/etc/init.d/ssh restart && /etc/init.d/centrifydc restart`


=== pam meghekkelése ('''CSAK AKKOR kell, ha valami miatt nem tudnánk sshzni''') ===

/etc/pam.d/common-auth-ban ez az elején legyen (különben nem fog menni az ssh):

.. TODO: Indent lines, set language: Example .. code-block:: python
# lines inserted by Centrify Direct Control (CentrifyDC 4.4.3-464)
auth       sufficient     pam_centrifydc.so try_first_pass
auth       requisite      pam_centrifydc.so deny

