
* A gépek elérhetőek Internetről.

   * IPv6-on közvetlenül
   * IPv4-en konfigurálható port forwarddal (szükség szerint publikus cím)
   * A szabálytalan hálózati aktivitás (torrent, ssh scan stb.) másodperceken belül tiltásra kerül
   * Gépek között és internet felé is legalább gigabites kapcsolat minden gépen 

* A laborklienssel a gépekre egy kattintással kapcsolódni lehet
* A felhasználói felület

   * magyar nyelven is elérhető
   * könnyebben kezelhető
   * szebb

* A sablonok létrehozása

   * önkiszolgáló
   * könnyen kezelhető

* A csoportok

   * vannak
   * Neptunból automatikusan létrejönnek
   * Kézzel is létrehozhatóak, módosíthatóak

* Van központi adattár

   * könnyen elérhető webről is
   * minden vendégrendszeren automatikusan csatolásra kerül

* Gépek felfüggeszthetőek/folytathatók